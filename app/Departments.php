<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Departments extends Model
{
    protected $fillable = [
        'department_name'
    ];
	
	public function departmentUsers()
	{
		return $this->hasMany('App\DepartmentUsers');
	}
	  
	  
}
