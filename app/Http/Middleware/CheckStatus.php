<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\User;

class CheckStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
         $response = $next($request);

        //If the status is not approved redirect to login 

        if(Auth::check() && Auth::user()->status != '1'){
			$user = User::where('id',Auth::user()->id)->first();
			$auth_user = User::with('comapnyDetail')->where('added_by',$user->added_by)->first();
            Auth::logout();

            return redirect()->route('login',$auth_user->comapnyDetail['subdomain'])->with('message', 'Your account is not activated yet');

        }

        return $response;

    }
    
}
