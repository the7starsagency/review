<?php

namespace App\Http\Middleware;

use Closure;
use App\CompanyDetails;
use Session;

class Subdomain
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		
		
        $company_name = $request->route('company');
	   
        $company = CompanyDetails::where('subdomain', '=', $company_name)->first();
		
        if( $company )
        {
			return $next($request);
			
        } else {
			
            return abort(404);
        }
    }
}
