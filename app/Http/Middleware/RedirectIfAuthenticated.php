<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\User;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
		
        if (Auth::guard($guard)->check()) {
			
			
			
			//echo "<pre>";print_r(Auth::user());die;
			
			/*$user = User::with('comapnyDetail')->find(Auth::user()->id);
			
            return redirect()->route('home',$user->comapnyDetail['subdomain']);*/
			
			//return redirect('/dashboard');
			
			
			return redirect('/dashboard');
        }

        return $next($request);
    }
}
