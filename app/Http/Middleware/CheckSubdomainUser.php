<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use Auth;

class CheckSubdomainUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
		$user_id = Auth::user()->id;
		
		$company_name = $request->route('company');
		
		$admin_user = User::whereHas('comapnyDetail', function($q)  use ($company_name){
					$q->where('subdomain', $company_name);
				})->first();
				
				
		
		if( $admin_user->id == $user_id )
		{
			$user_data = User::whereHas('comapnyDetail', function($q)  use ($company_name){
					$q->where('subdomain', $company_name);
				})->where('added_by', $user_id)->first();
		} else{
			$user_data = User::where(['id' => $user_id,'added_by' => $admin_user->id])->first();
		}
		
				
		if(!empty($user_data))
		{			
				
			return $next($request);
		} else {
			 Auth::logout();
			 return abort(404);
		}
    }
}
