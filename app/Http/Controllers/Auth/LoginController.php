<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Illuminate\Http\Request;
use App\CompanyDetails;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['guest'])->except('logout');
		
		
    }
	
	public function index(){
		
		
		return view('auth.login');
	}
	
	
	
	public function logout(Request $request,$company_name) {
		//echo "dd";die;
		 session()->flush();
		 Auth::logout();
		 return redirect()->route('login',$company_name);
	  
	}
	
	public function validateSubdomain(Request $request)
	{
		
		if( isset($request['subdomain']) && $request['subdomain'] != "")
		{
			$subdomain = CompanyDetails::where('subdomain','=',$request['subdomain'])->first();
			
			if( !empty($subdomain) )
			{
				return 1;
			}else {
				return 0;
			}
		}
		 
	}
	
	
	public function showLoginForm($company_name)
	{
		return view('auth.login')->with('company_name',$company_name);
		
	}
	public function companyVerify($company_name,$token)
	{	
		$user = User::checkUserAuthentication($token);
		if(!empty($user))
		{
			if( $user->status == '1' )
			{
				return redirect()->route('login',$company_name)->with('message','Account is already activated. Please login now');
			}
			 $user->status = '1';
			 $user->save();
			 return redirect()->route('login',$company_name)->with('message','Your account is verified.Please login now');
		}			
	}
	
	
	public function thank($comapny_name)
	{
		return view('thank')->with('company_name',$comapny_name);
	}
	
	
	
}
