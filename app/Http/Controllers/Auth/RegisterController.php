<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Countries;
use App\CompanyDetails;
use App\CommonFunctions;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
		
		
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
	 
		

	public function showRegistrationForm()
    {

		$countries = Countries::orderBy('name')->get();
		return view('auth.register', compact('countries'));

	}

    protected function validator(array $data)
    {
		
        return Validator::make($data, [
            'name' 				=> ['required', 'string', 'max:255'],
            'email' 			=> ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' 			=> ['required', 'string', 'min:8', 'confirmed'],
			'company_name' 		=> ['required'],
			'subdomain' 		=> ['required'],
          
        ], [

            'company_name.required' => 'Company Name is required',
            'subdomain.required'	 => 'Subdomain is required',



        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {

		$token			 	    = md5(date('ymdhis').rand(0,5).$data['email']);
		
		$user = config('roles.models.defaultUser')::create([
            'name' 			=> $data['name'],
            'email' 		=> $data['email'],
            'password' 		=> Hash::make($data['password']),
			'password_reset_token' => $token,
		]);
		
		if( !empty($user))
		{
			$user->added_by = $user->id;
			$user->save();
			
			$user->attachRole(1);
			$user->attachRole(2);
			
			if (  isset( $data['profile_picture'] ) ) {
				
				$user_id 				= $user['id'];
				$uploads_dir 			= CommonFunctions::getAdMediaFolderPath($user_id);
				$file_name 				= $data['profile_picture'] ? CommonFunctions::uploadFile( $data['profile_picture'], $uploads_dir ) : '';
				$user->profile_picture 	= $file_name;
				$user->save();
			}
				

				CompanyDetails::create([
					'company_id' 	=> $user['id'],
					'company_name'	=> $data['company_name'],
					'subdomain'		=> $data['subdomain'],
					'country_id' 	=> $data['country_id'],
					'city' 			=> $data['city'],
					'address' 		=> $data['address'],
				]);
				
			$data = array(
						'email'		=> $user->email,
						'token' 	=> $token,
						'subdomain'  => $data['subdomain'],
						'subject'	=> "Company Verifcation"
						);
					
				CommonFunctions::sendLarvelDefaultMail('emails.admin-verification',$data);	
			
		}
        return $user;

    }
	
	public function register(Request $request)
	{
			
		$this->validator($request->all())->validate();

		event(new Registered($user = $this->create($request->all())));
		
		return redirect()->route('company.thank-you')->with('message', 'Please verify your email to activate your account');
		
	}
	
	
	public function thankYou()
	{
		return view('/thank-you');
	}

	
}
