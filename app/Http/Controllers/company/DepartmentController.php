<?php

namespace App\Http\Controllers\company;

use App\Http\Controllers\Controller;
use App\Department;
use Illuminate\Http\Request;
use Datatables;
use Auth;
use App\User;
use App\Team;
use App\Countries;
use App\DepartmentTeams;
use App\CommonFunctions;
use App\DepartmentUsers;
use Illuminate\Support\Facades\Hash;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request,$company_name)
    {
		$user_id = Auth::id();
        $department_detail = Department::where('added_by',$user_id)->get();

		if ( $request->ajax()) {

		    return Datatables::of($department_detail)->addIndexColumn()->addColumn('action', function($department) use($company_name){

				return '<a href="'.route('department.edit',[$company_name,$department->id]).'" class="btn btn-sm default" data-type="cancel" ><i class="material-icons">edit</i></a><a href="javascript:void()" data-id = "'.$department->id.'"  class="btn btn-sm default dep-delete" data-type="cancel" ><i class="material-icons">delete</i></a>';

			})->make(true);
		}

		return view('company.department.list')->with('company_name',$company_name);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($company_name)
    {
        $user_id = Auth::id();

		$people_data = User::whereHas('roles', function($q)  {
			$q->where('slug', 'people');})->where('added_by',$user_id)->get();


		$manager_data = User::whereHas('roles', function($q)  {
			$q->where('slug', 'manager');})->where('added_by',$user_id)->get();

		$teams_data =  Team::whereDoesntHave('teamDepartments')->where('added_by',$user_id)->get();





		return view('company.department.add')->with([
												'teams_data' 	=> $teams_data,
												'people_data' 		=> $people_data,
												'manager_data' 		=> $manager_data,
												'company_name' 		=> $company_name
											]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$company_name)
    {
        $request->validate([
			'department_name' 		=> ['required', 'string', 'max:255'],
		]);

		$user_id = Auth::id();
		$data = array();
		$data = $request->all();
		$data['added_by'] = $user_id;

		$id  = Department::create($data)->id;

		if( $id ){

			if(isset($request['teams']) && count($request['teams']) > 0 )

				{
					foreach( $request['teams'] as $team )
					{
						DepartmentTeams::departmentTeams( $id, $team );
					}

				}

				if(isset($request['department_lead']) && count($request['department_lead']) > 0 )

				{
					foreach( $request['department_lead'] as $dep_lead )
					{
						DepartmentUsers::departmentUsers( $id, $dep_lead );
					}

				}

				if(isset($request['department_members']) && count($request['department_members']) > 0 )

				{
					foreach( $request['department_members'] as $dep_mem )
					{
						DepartmentUsers::departmentUsers( $id, $dep_mem );
					}

				}

			return redirect()->route('department',$company_name)->with('message', 'Department Added Successfully');
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Departments  $departments
     * @return \Illuminate\Http\Response
     */
    public function show($company_name, $id)
    {

        $user_id = Auth::id();

		$people_data = User::whereHas('roles', function($q)  {
			$q->where('slug', 'people');})->where('added_by',$user_id)->get();


		$manager_data = User::whereHas('roles', function($q)  {
			$q->where('slug', 'manager');})->where('added_by',$user_id)->get();

		$teams_data =  Team::with('teamDepartments')->get();
		//echo "<pre>";print_r($teams_data);die;
		$department_data = Department::with(['departmentUsers','departmentTeams'])->where(['added_by' => $user_id, 'id' => $id])->first();

		$select_team = array();
		if(isset($department_data->departmentTeams)){
			foreach( $department_data->departmentTeams as $key=>$selected_team )
			{
				$select_team[$key] = $selected_team->team_id;
			}
		}

		$select_users = array();
		if(isset($department_data->departmentUsers)){
			foreach( $department_data->departmentUsers as $key=>$selected_users )
			{
				$select_users[$key] = $selected_users->user_id;
			}
		}

		return view('company.department.edit')->with([
												'teams_data' 		=> $teams_data,
												'people_data' 		=> $people_data,
												'manager_data' 		=> $manager_data,
												'department' 	    => $department_data,
												'select_team' 	    => $select_team,
												'select_users' 	    => $select_users,
												'department_id' 	    => $id,
												'company_name' 	    => $company_name
											]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Departments  $departments
     * @return \Illuminate\Http\Response
     */
    public function edit(Departments $departments)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Departments  $departments
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Department $department,$company_name)
    {


        $request->validate([
			'department_name' 		=> ['required', 'string', 'max:255'],
		]);

		$department = Department::find($request['department_id']);
		$department->department_name = $request['department_name'];
		$department->save();

		$users_array = array();

		if( isset($request['department_lead']) && count($request['department_lead']) > 0 ) {


			foreach( $request['department_lead'] as $key=>$dep_lead )
			{
				$users_array[$key] = $dep_lead;
			}
		$new_key = count($request['department_lead']);
		} else{
			$new_key = 0;
		}

		if( isset($request['department_members']) &&  count($request['department_members']) > 0 )
				{
					foreach( $request['department_members'] as $key=>$dep_member )
					{
						$users_array[$key+$new_key] = $dep_member;
					}
				}

		if( count($users_array) > 0 )
		{
			 DepartmentUsers::where( 'department_id',$department->id )->get()->each->delete();

			foreach( $users_array as $user_id )
			{

				DepartmentUsers::departmentUsers( $department->id, $user_id);
			}

		} else{
			DepartmentUsers::where( 'department_id',$department->id )->get()->each->delete();
		}

		if(isset($request['teams']) && $request['teams'] != "" ) {

			DepartmentTeams::where('department_id',$department->id)->get()->each->delete();

			foreach( $request['teams'] as $team_id )
			{
				DepartmentTeams::departmentTeams( $department->id, $team_id );
			}

		} else{

			DepartmentTeams::where('department_id',$department->id)->get()->each->delete();

		}

		return redirect()->route('department',$company_name)->with('message', 'Department Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Departments  $departments
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {

		if(isset( $request['id'] ) && $request['id'] != "")
		{

			$dep = Department::find($request['id']);
			$dep->departmentUsers()->delete();
			$dep->departmentTeams()->delete();
			$dep->delete();
			return 1;

		}

		return 0;

    }
}
