<?php

namespace App\Http\Controllers\company;

use App\ReviewTemplate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Datatables;
use App\Attribute;
use App\ReviewTemplateAttribute;

class ReviewTemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $company_name)
    {
        $user_id = Auth::id();
        $review_template = ReviewTemplate::where('created_by',$user_id)->get();

		if ( $request->ajax()) {

		    return Datatables::of($review_template)->addIndexColumn()->addColumn('action', function($template) use($company_name){

				return '<a href="'.route('template.edit',[$company_name,$template->id]).'" class="btn btn-sm default" data-type="cancel" ><i class="material-icons">edit</i></a><a href="javascript:void()" data-id = "'.$template->id.'"  class="btn btn-sm default dep-delete" data-type="cancel" ><i class="material-icons">delete</i></a>';

			})->make(true);
		}
        return view('company.template.list')->with(['company_name' => $company_name]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($company_name)
    {
        $user_id = Auth::id();
        $review_template =  ReviewTemplate::where('created_by',$user_id)->get();
        $attribute = Attribute::where('created_by', $user_id)->get();

        return view('company.template.add')->with([
            'review_template' 	=> $review_template,
            'company_name'      => $company_name,
            'attribute'         => $attribute

        ]);


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request,$company_name )
    {
		
         $request->validate([
			'name' 		=> ['required', 'string', 'max:255'],
		]);
		
		$user_id = Auth::id();
		$id = ReviewTemplate::saveReviewTemplate( $user_id,$request['name'],$request['description']  );
		if( $id > 0 ) {
			if( isset( $request['attributes'] ) && count( $request['attributes'] ) > 0 ) 
				{
					$key = 1;
					
					foreach( $request['attributes'] as $attr )
					{
						
						ReviewTemplateAttribute::saveReviewTemplateAttribute( $id, $attr, $key );
					$key++;}
				}
			
			return redirect()->route('template',$company_name)->with('message', 'Template Added Successfully');
		}
		
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ReviewTemplate  $reviewTemplate
     * @return \Illuminate\Http\Response
     */
    public function show( $company_name, $id )
    { 
		
        $user_id = Auth::id();
		$count = 0;
		
                
		
		$review_temp = ReviewTemplate::with(['reviewTemplateAttributeValues','reviewTemplateAttributeValues.reviewTemplateAttribute'])->find( $id );
	
		if(isset($review_temp->reviewTemplateAttributeValues))
		{
			$count = count($review_temp->reviewTemplateAttributeValues);
		}
		$attribute = Attribute::where('created_by', $user_id)->get();
		

		return view('company.template.edit')->with([
												'review_template_id'           => $id,
												'company_name'          	   => $company_name,
												'review_temp'				   => $review_temp,
												'attribute'					   => $attribute,
												'count'    					   => $count	

											]);
    }

   

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ReviewTemplate  $reviewTemplate
     * @return \Illuminate\Http\Response
     */
    public function edit(ReviewTemplate $reviewTemplate)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ReviewTemplate  $reviewTemplate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $company_name)
    {
		
       $request->validate([
			'name' 		=> ['required', 'string', 'max:255'],
		]);
		
		$user_id = Auth::id();
		$review_temp = ReviewTemplate::find($request['id']);
		$review_temp->name = 	$request['name'];
		$review_temp->description = 	$request['description'];
		$review_temp->save();
		if( $review_temp->id > 0 )
		{
			if( isset( $request['attributes'] ) && count( $request['attributes'] ) > 0 ) 
				{
					$temp_values = ReviewTemplateAttribute::where( 'review_template_id',$review_temp->id )->get()->each->delete();
					$key = 1;
					
					foreach( $request['attributes'] as $attr )
					{
						
						ReviewTemplateAttribute::saveReviewTemplateAttribute( $review_temp->id, $attr, $key );
					$key++;}
				} else{
					$temp_values = ReviewTemplateAttribute::where( 'review_template_id',$review_temp->id )->get()->each->delete();
				}
			
			return redirect()->route('template',$company_name)->with('message', 'Template Updated Successfully');
			
		}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ReviewTemplate  $reviewTemplate
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
       if(isset($request['id']) && $request['id'] != "")
		{
			$review_template = ReviewTemplate::with('reviewTemplateAttributeValues')->find($request['id']);
			
			if(isset( $review_template->reviewTemplateAttributeValues ) && count($review_template->reviewTemplateAttributeValues) > 0 )
			{
				$review_template->reviewTemplateAttributeValues()->delete();
			}
			
			$review_template->delete();
			return 1;

		}

		return 0;
    }
   

    public function addAttribute(Request $request,$company_name)
    {

        if( $request['id'] ){
            $attribute = Attribute::find( $request['id']);

            if(!empty($attribute)){

                $data['attribute_name'] = $attribute->attribute_name;
                $data['id'] = $attribute->id;
                $data['response'] = 1;
                return $data;
            } else{
                return 0;
            }
        } else{

        return 0;
    }





    }
}
