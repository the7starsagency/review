<?php

namespace App\Http\Controllers\company;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Datatables;
use App\User;
use App\Countries;
use App\Departments;
use App\Team;
use App\CommonFunctions;
use App\DepartmentUsers;
use App\TeamUsers;
use App\DepartmentTeams;
use App\CompanyDetails;
use Illuminate\Support\Facades\Hash;
use Auth;
use Session;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request, $company_name, $type)
    {

		$userId = Auth::id();
		if( $type == 'people' ) {

			$user_data = User::where('added_by', $userId)->get();

		} else {


			$user_data = User::with('roles')->whereHas('roles', function($q)  use ($type){
				$q->where('slug', $type);
			})->where('added_by', $userId)->get();



		}

		if ( $request->ajax()) {

		    return Datatables::of($user_data)->addIndexColumn()->addColumn('action', function($user) use($company_name,$type){

				return '<a href="'.route('users.show',[$company_name,$type,$user->id]).'" class="btn btn-sm default" data-type="cancel" ><i class="material-icons">edit</i></a><a href="javascript:void()" data-id = "'.$user->id.'" class="btn btn-sm default data-delete" data-type="cancel" ><i class="material-icons">delete</i></a>';

			})->make(true);
		}

		return view('company.users.list')->with(['type' => $type, 'company_name' => $company_name]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($company_name,$type)
    {
        $user_id = Auth::id();

        $departments = Departments::where('added_by',$user_id)->get();

        $teams = Team::where('added_by',$user_id)->get();

        $people_data = User::with('peopleRole','managerRole','adminRole')->
        whereHas('peopleRole')->doesnthave('adminRole')->doesnthave('managerRole')->where('added_by',$user_id)->get();

        if( $type == 'manager' ){
            return view('company.users.manager-add')->with(['type' => $type,'people_data' => $people_data,'company_name' => $company_name]);
        } else{
            return view('company.users.add')->with(['type' => $type,'departments' => $departments, 'teams' => $teams,'company_name' => $company_name]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$company_name,$type)
    {

        if( isset( $request['email'] ) ) {

			$email_arr	=  explode(",",$request['email']);
			$userId 	= Auth::id();

			foreach($email_arr as $email )
			{
				$token			 			= md5(date('ymdhis').rand(0,5));
				$user 			 			= New User();
				$user->email	 			= $email;
				$user->password  			= Hash::make($email);
				$user->status   			= '0';
				$user->password_reset_token = $token;
				$user->added_by 			= $userId;
				$user->Save();
				if( $type == 'people'){

					$user->attachRole(2);

					//$role		                = config('roles.models.defaultUser')::find(2);

				} else if($type == 'manager'){

					$user->attachRole(3);
					$user->attachRole(2);
					//$role		                = config('roles.models.defaultUser')::find(3);

				}


				if( isset($request['departments']) && count($request['departments']) > 0 )
				{
					foreach( $request['departments'] as $department_id )
					{
						DepartmentUsers::departmentUsers( $department_id, $user->id);
					}
				}

				if( isset($request['teams']) && count($request['teams']) > 0 )
				{
					foreach( $request['teams'] as $team_id )
					{
						TeamUsers::teamUsers( $user->id, $team_id);
					}
				}

				$data = array(
							'email'		=> $email,
							'token' 	=> $token,
							'company_name'=>$company_name,
							'subject'	=> "User Verifcation"
							);

				CommonFunctions::sendLarvelDefaultMail('emails.reset-password',$data);
			}
				return redirect()->route('users',[$company_name,$type])->with('message', ucfirst($type).' Added Successfully');

		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($company_name,$type,$id)
    {
		$user_detail = User::with(['usersInDepartment','usersInTeams','roles'])->find( $id );
		$user_id = Auth::id();
		$departments = Departments::where('added_by',$user_id)->get();
		$teams = Team::where('added_by',$user_id)->get();
		$select_department = array();

		$manager_role = array();
		foreach( $user_detail->roles as $key=>$roles)
		{

			$manager_role[$key] = $roles['pivot']->role_id;
		}

		foreach( $user_detail->usersInDepartment as $key=>$selected_dep )
		{
			$select_department[$key] = $selected_dep->department_id;
		}
		$select_team = array();

		foreach( $user_detail->usersInTeams as $key=>$selected_team )
		{
			$select_team[$key] = $selected_team->team_id;
		}

        return view('company.users.edit')->with([
											'type' 			=> $type,
											'user_id' 		=> $id,
											'user' 			=> $user_detail,
											'departments' 	=> $departments,
											'teams'			=> $teams,
											'select_department'			=> $select_department,
											'select_team'			=> $select_team,
											'manager_role'			=> $manager_role,
											'company_name'			=> $company_name,
										]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$company_name,$type)
    {
		$data = $request->all();

		if( isset($request['user_id'])  )
		{	$user = User::find($request['user_id']);


			if( $type == "people"){
				if(isset($request['is_manager']) && $request['is_manager'] == 1)
				{
					$user->attachRole(3);

				} else{
					$user->detachRole(3);
				}
			}

		    if( isset($request['departments']) && count($request['departments']) > 0 )
					{
						DepartmentUsers::where( 'user_id',$request['user_id'] )->get()->each->delete();

						foreach( $request['departments'] as $department_id )
						{
							DepartmentUsers::departmentUsers( $department_id, $request['user_id']);
						}
					} else{
						DepartmentUsers::where( 'user_id',$request['user_id'] )->get()->each->delete();
					}

					if( isset($request['teams']) && count($request['teams']) > 0 )
					{
						TeamUsers::where( 'user_id',$request['user_id'] )->get()->each->delete();

						foreach( $request['teams'] as $team_id )
						{
							TeamUsers::teamUsers( $request['user_id'], $team_id);
						}
					}
					else{
						TeamUsers::where( 'user_id',$request['user_id'] )->get()->each->delete();
					}
			$user->fill( $data )->save();
		}
		return redirect()->route('users',[$company_name,$type])->with('message', 'User Updated Successfully');
	}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if(isset($request['id']))
		{
			$user = User::with(['usersInDepartment','usersInTeams'])->find($request['id']);

			if( !empty( $user->usersInDepartment )){
				$user->usersInDepartment->each->delete();
			}
			if( !empty( $user->usersInTeams )){
				$user->usersInTeams->each->delete();
			}
			$user->roles[0]['pivot']->delete();
			$user->delete();
			return 1;

		}

		return 0;
    }

	public function validateEmail(Request $request)
    {
        if( isset($request->email )){

			$user = User::where('email','=',$request->email)->first();

			if( !empty($user )) {

				return 1;

			} else {

				return 0;
			}
		}
    }

	public function userResetToken($company_name,$token)
	{
		$user = User::checkUserAuthentication( $token );
		return view('auth.passwords.user-reset')->with( ['user' => $user, 'company_name' => $company_name] );
	}

	public function userResetPassword(Request $request,$company_name )
	{

		$request->validate([
			'name' 		=> ['required', 'string', 'max:255'],
            'password' 	=> ['required', 'string', 'min:8', 'confirmed'],
		]);

		$user = User::where('id','=', $request->user_id )->first();
		$user->name = $request->name;
		$user->password = Hash::make($request->password);
		$user->email_verified_at = date('Y-m-d h:i:s');
		$user->status = '1';
		$user->password_reset_token = Null;
		$user->save();
		return redirect()->route('login',$company_name)->with('message', 'Thank you for activating your account. You can login now.');


	}

	public function chnageRole(request $request)
	{
		$user = User::find($request['user_id']);
		if( $request['status'] == 1 )
		{
			$user->attachRole(3);
			return 1;

		} else {

			$user->detachRole(3);
			return 0;
		}

	}

	public function dashHome(){
		//echo "ddd";die;
		$user = User::with('comapnyDetail')->find(Auth::user()->id);
		if(!empty($user)){


			return redirect()->route('home',$user->comapnyDetail['subdomain']);
		} else{
			//echo "else";die;
			return redirect()->route('/register');
		}

    }
    public function upgradePeople(Request $request, $company_name )
    {
        if( isset($request['upgarde_people']) )
        {
            foreach($request['upgarde_people'] as $people_id )
            {
                $user =  User::find( $people_id );
                $user->attachRole(3);

            }
            return redirect()->route('users',[$company_name,'manager'])->with('message', 'People Upgrated Successfully');
        }
	}
	
	public function profile( $company_name ){

        if( $company_name && Auth::user()->id > 0 ){

            $profile = User::with('comapnyDetail')->find( Auth::user()->id );
            $countries = Countries::orderBy('name')->get();
            return view('company.profile')->with([ 'company_name' => $company_name,'countries' => $countries, 'profile' => $profile ]);

        }
    }

    public function updateProfile( Request $request, $company_name ){

		$data = $request->all();
		$user_details = CompanyDetails::where('company_id',Auth::user()->id)->first();

		if( $user_details != NULL ){
			$request->validate([
				'name' 		=> ['required'],
				'company_name' => ['required','unique:company_details,company_name,'.$user_details->id],
				'subdomain'     => ['required','unique:company_details,subdomain,'.$user_details->id],
			]);
		}else{
			$request->validate([
				'name' 		=> ['required'],
				'company_name' => ['required'],
				'subdomain'     => 'required'
			]);
		}
        

		$user = User::find( Auth::user()->id );

        if( $user !== NULL ){

            if (  isset( $data['profile_picture'] ) ) {
				
                $uploads_dir 			= CommonFunctions::getAdMediaFolderPath( Auth::user()->id );
                $file_name 				= $data['profile_picture'] ? CommonFunctions::uploadFile( $data['profile_picture'], $uploads_dir ) : '';
                $data['profile_picture'] = $file_name;
                
            }
            
            if( $user->fill( $data )->save() ){ // user table update

                if( $user_details ){

					if( $user_details->fill( $data )->save() ){

						return redirect()->route('profile',$data['subdomain'])->with('message', 'Company profile Updated Successfully');

					}
					return redirect()->route('profile',$company_name)->with('error', 'Something went wrong');
				
                }
			}
			return redirect()->route('profile',$company_name)->with('error', 'Something went wrong');
        }
		return redirect()->route('profile',$company_name)->with('error', 'Something went wrong');
        
	}
	
	public function validateSubdomain( Request $request ){

		if( isset($request['subdomain']) && $request['subdomain'] != "")
		{
			$company_details = CompanyDetails::where( 'company_id',Auth::user()->id )->first();

			if( $company_details != NULL ){
				$subdomain = CompanyDetails::where([ ['subdomain','=',$request['subdomain']],['id', '!=', $company_details->id ] ])->first();
			}else{
				$subdomain = CompanyDetails::where('subdomain','=',$request['subdomain'])->first();
			}
			if( !empty($subdomain) )
			{
				return 1;
			}else {
				return 0;
			}
		}
	}

}
