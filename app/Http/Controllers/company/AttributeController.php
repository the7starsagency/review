<?php

namespace App\Http\Controllers\company;

use App\Attribute;
use App\AttributeValues;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Datatables;
use Auth;


class AttributeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $company_name )
    {
        $user_id = Auth::id();
        $attribute_detail = Attribute::where('created_by',$user_id)->get();

		if ( $request->ajax()) {

		    return Datatables::of($attribute_detail)->addIndexColumn()->addColumn('action', function($attribute) use($company_name){

				return '<a href="'.route('attribute.edit',[$company_name,$attribute->id]).'" class="btn btn-sm default" data-type="cancel" ><i class="material-icons">edit</i></a><a href="javascript:void()" data-id = "'.$attribute->id.'"  class="btn btn-sm default dep-delete" data-type="cancel" ><i class="material-icons">delete</i></a>';

			})->make(true);
		}

		return view('company.attribute.list')->with('company_name',$company_name);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create( $company_name )
    {
        $user_id = Auth::user()->id;
        $attribute = Attribute::where('created_by', $user_id)->get();
        return view('company.attribute.add')->with( ['company_name' => $company_name,'attribute' => $attribute] );

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$company_name)
    {

        $request->validate([
			'attribute_name' 	=> ['required', 'string', 'max:255'],

        ]);

        $user_id = Auth::user()->id;

        $attribute = New Attribute();
        $attribute->attribute_name = $request['attribute_name'];
        $attribute->description   = $request['description'];
        if( isset($request['evaluation']) )
        {
            $attribute->is_evaluation_values = $request['evaluation'];
        }

        $attribute->created_by = $user_id;
        $attribute->save();

        if( $attribute->id > 0 )
        {
            if( isset($request['attrValues']) && count($request['attrValues']) > 0 )
            {
				$order = 1;
                foreach( $request['attrValues'] as $key=>$val )
                {
                    if( $val['values'] != "" || $val['descrption'] != "" )
                    {

                        $attribute_values = New AttributeValues();
                        $attribute_values->attribute_id = $attribute->id;
                        $attribute_values->score = $val['values'];
                        $attribute_values->description = $val['descrption'];
                        $attribute_values->order = $order;
                        $attribute_values->save();
                        $order++;
					}

                }

               //
             }

         }

         return redirect()->route('attribute',$company_name)->with('message', 'Attribute created successfully.');



    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Attribute  $attribute
     * @return \Illuminate\Http\Response
     */
    public function show($company_name, $id)
    {

        if( $id )
        {
            $count_value = 0;
            $attribute = Attribute::with(['attributeValues'])->find($id);

			if(!empty($attribute))
            {
                if(isset($attribute->attributeValues) && count($attribute->attributeValues) > 0 )
                {
                    $count_value = count($attribute->attributeValues);
                }
				return view('company.attribute.edit')->with([

											'company_name' 	    => $company_name,
                                            'attribute'         =>  $attribute,
                                            'count_value'      => $count_value,
										]);
			}
		}


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Attribute  $attribute
     * @return \Illuminate\Http\Response
     */
    public function edit(Attribute $attribute)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Attribute  $attribute
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Attribute $attribute,$company_name)
    {
        $request->validate([
			'attribute_name' 	=> ['required', 'string', 'max:255'],

        ]);


        $attribute = Attribute::with(['attributeValues'])->find( $request['attribute_id']);
        $attribute->attribute_name = $request['attribute_name'];
        $attribute->description   = $request['description'];
        if( isset($request['evaluation']) )
        {
            $attribute->is_evaluation_values = $request['evaluation'];
        } else{
            $attribute->is_evaluation_values = '0';
        }

        $attribute->save();

        if( $attribute->id > 0 )
        {
            if( isset($request['attrValues']) && count($request['attrValues']) > 0 )
            {
				$order = 1;
                $attribute->attributeValues->each->delete();

                foreach( $request['attrValues'] as $key=>$val )
                {
                    if( $val['values'] != "" || $val['descrption'] != "" )
                    {

                        $attribute_values = New AttributeValues();
                        $attribute_values->attribute_id = $attribute->id;
                        if( isset($request['evaluation']) ){
                            $attribute_values->score = $val['values'];
                        }
                        $attribute_values->description = $val['descrption'];
                        $attribute_values->order = $order;
                        $attribute_values->save();
                        $order++;
					}

                }

               //
            } else{
                $attribute->attributeValues->each->delete();
            }

        }

         return redirect()->route('attribute',$company_name)->with('message', 'Attribute created successfully.');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Attribute  $attribute
     * @return \Illuminate\Http\Response
     */
    public function destroy( Request $request )
    {
        if(isset($request['id']))
		{
			$attribute = Attribute::with(['attributeValues'])->find($request['id']);

            /*if( isset( $attribute->attributeValues ) && count( $attribute->attributeValues ) > 0 )
            {
               $attribute->attributeValues->each->delete();
            }*/

            $attribute->delete();

            return 1;

		}

		return 0;
    }


    public function cloneAttribute(Request $request)
    {
        if( $request['id'] != "")
        {
            $attribute = Attribute::with(['attributeValues'])->find($request['id']);

            if(count($attribute->attributeValues) > 0)
            {
                $data['attribute'] = $attribute->attributeValues;
                $data['is_value'] = $attribute->is_evaluation_values;
                return $data;

            } else {
                return -1;
            }

        } else {
            return 0;

    }
}
}
