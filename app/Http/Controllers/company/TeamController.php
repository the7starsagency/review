<?php

namespace App\Http\Controllers\company;

use App\Http\Controllers\Controller;
use App\Team;
use Illuminate\Http\Request;
use Datatables;
use App\User;
use App\TeamUsers;
use App\Departments;
use App\DepartmentTeams;
use Auth;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request,$company_name )
    {
		$user_id = Auth::id();
        $team_detail = Team::where('added_by',$user_id)->get();

		if ( $request->ajax()) {

		    return Datatables::of($team_detail)->addIndexColumn()->addColumn('action', function($team) use ($company_name){

				return '<a href="'.route('team.edit',[$company_name,$team->id]).'" class="btn btn-sm default" data-type="cancel" ><i class="material-icons">edit</i></a><a href="javascript:void()" data-id = "'.$team->id.'"  class="btn btn-sm default team-delete" data-type="cancel" ><i class="material-icons">delete</i></a>';

			})->make(true);
		}

		return view('company.team.list')->with('company_name',$company_name);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($company_name)
    {

		$user_id = Auth::id();
		$people_data = User::whereHas('roles', function($q)  {
			$q->where('slug', 'people');})->where('added_by',$user_id)->get();


		$manager_data = User::whereHas('roles', function($q)  {
			$q->where('slug', 'manager');})->where('added_by',$user_id)->get();

		$deapartment =  Departments::where('added_by',$user_id)->get();

        return view('company.team.add')->with([
												'deapartment' 	=> $deapartment,
												'people_data' 		=> $people_data,
												'manager_data' 		=> $manager_data,
												'company_name' 		=> $company_name,
											]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$company_name)
    {
        $request->validate([
			'team_name' 		=> ['required', 'string', 'max:255'],
		]);

		$user_id = Auth::id();
		$data = array();
		$data = $request->all();
		$data['added_by'] = $user_id;

		$id  = Team::create($data)->id;


		if( $id ) {

			if(isset($request['department']) && $request['department'] != "" ) {

				DepartmentTeams::departmentTeams( $request['department'], $id );

			}
			if( isset($request['people']) && count($request['people']) > 0 )
				{
					foreach( $request['people'] as $key=>$people )
					{
						TeamUsers::teamUsers( $people, $id );
					}
				}

			if( isset($request['manager']) &&  count($request['manager']) > 0 )
				{
					foreach( $request['manager'] as $key=>$manager )
					{
						TeamUsers::teamUsers( $manager, $id );
					}
				}

		}

		return redirect()->route('team',$company_name)->with('message', 'Team Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function show($company_name, $id )
    {

		$user_id = Auth::id();

		$people_data = User::whereHas('roles', function($q)  {
			$q->where('slug', 'people');})->where('added_by',$user_id)->get();


		$manager_data = User::whereHas('roles', function($q)  {
			$q->where('slug', 'manager');})->where('added_by',$user_id)->get();

		$deapartment =  Departments::where('added_by',$user_id)->get();

		$team_data = Team::with(['teamUsersPeople','teamUsersManager','teamDepartments'])->where(['added_by' => $user_id, 'id' => $id])->first();

		$select_people = array();
		if(isset($team_data->teamUsersPeople))
		{
			foreach( $team_data->teamUsersPeople as $key=>$selected_people )
			{
				$select_people[$key] = $selected_people->user_id;
			}
		}
		$select_manager = array();
		if(isset($team_data->teamUsersManager)){
			foreach( $team_data->teamUsersManager as $key=>$selected_manager )
			{
				$select_manager[$key] = $selected_manager->user_id;
			}
		}


		return view('company.team.edit')->with([
												'deapartment' 		=> $deapartment,
												'people_data' 		=> $people_data,
												'manager_data' 		=> $manager_data,
												'team'				=> $team_data,
												'select_people'		=> $select_people,
												'select_manager'    => $select_manager,
												'team_id'           => $id,
												'company_name'           => $company_name,

											]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function edit(Team $team)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Team $team,$company_name)
    {
		$request->validate([
			'team_name' 		=> ['required', 'string', 'max:255'],
		]);

		$team = Team::find($request['team_id']);
		$team->team_name = $request['team_name'];
		$team->save();

		if(isset($request['department']) && $request['department'] != "" ) {

			$team_dep = Team::find($team->id);
			if($team_dep->teamDepartments != Null){

				$team_dep->teamDepartments->delete();
			}
			DepartmentTeams::departmentTeams( $request['department'], $team->id );

		}
		$users_array = array();
		if( isset($request['people']) && count($request['people']) > 0 ) {


			foreach( $request['people'] as $key=>$people )
			{
				$users_array[$key] = $people;
			}
		$new_key = count($request['people']);
		} else{
			$new_key = 0;
		}

		if( isset($request['manager']) &&  count($request['manager']) > 0 )
				{
					foreach( $request['manager'] as $key=>$manager )
					{
						$users_array[$key+$new_key] = $manager;
					}
				}

		if( count($users_array) > 0 )
		{
				$team_users = TeamUsers::where( 'team_id',$team->id )->get()->each->delete();

			foreach( $users_array as $user_id )
			{

				TeamUsers::teamUsers( $user_id, $team->id );
			}

		} else{
			$team_users = TeamUsers::where( 'team_id',$team->id )->get()->each->delete();
		}

		return redirect()->route('team',$company_name)->with('message', 'Team Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {

		if(isset($request['id']) && $request['id'] != "")
		{
			$team = Team::find($request['id']);
			$team->teamUsers()->delete();
			$team->teamDepartments()->delete();
			$team->delete();
			return 1;

		}

		return 0;
    }
}
