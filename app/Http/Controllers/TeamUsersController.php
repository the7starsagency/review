<?php

namespace App\Http\Controllers;

use App\TeamUsers;
use Illuminate\Http\Request;

class TeamUsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TeamUsers  $teamUsers
     * @return \Illuminate\Http\Response
     */
    public function show(TeamUsers $teamUsers)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TeamUsers  $teamUsers
     * @return \Illuminate\Http\Response
     */
    public function edit(TeamUsers $teamUsers)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TeamUsers  $teamUsers
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TeamUsers $teamUsers)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TeamUsers  $teamUsers
     * @return \Illuminate\Http\Response
     */
    public function destroy(TeamUsers $teamUsers)
    {
        //
    }
}
