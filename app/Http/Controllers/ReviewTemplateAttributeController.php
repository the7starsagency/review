<?php

namespace App\Http\Controllers;

use App\ReviewTemplateAttribute;
use Illuminate\Http\Request;

class ReviewTemplateAttributeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ReviewTemplateAttribute  $reviewTemplateAttribute
     * @return \Illuminate\Http\Response
     */
    public function show(ReviewTemplateAttribute $reviewTemplateAttribute)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ReviewTemplateAttribute  $reviewTemplateAttribute
     * @return \Illuminate\Http\Response
     */
    public function edit(ReviewTemplateAttribute $reviewTemplateAttribute)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ReviewTemplateAttribute  $reviewTemplateAttribute
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ReviewTemplateAttribute $reviewTemplateAttribute)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ReviewTemplateAttribute  $reviewTemplateAttribute
     * @return \Illuminate\Http\Response
     */
    public function destroy(ReviewTemplateAttribute $reviewTemplateAttribute)
    {
        //
    }
}
