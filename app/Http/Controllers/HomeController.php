<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Countries;
use App\User;
use App\CompanyDetails;
use Auth;
use App\CommonFunctions;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
	 
	
	 
    public function __construct()
    {	
		
        $this->middleware(['auth']);
		//echo "ddd";die;
	 
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index( $company_name )
    {  
		
        return view('home')->with('company_name',$company_name);
    }
}
