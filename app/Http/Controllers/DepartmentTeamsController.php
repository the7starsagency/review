<?php

namespace App\Http\Controllers;

use App\DepartmentTeams;
use Illuminate\Http\Request;

class DepartmentTeamsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DepartmentTeams  $departmentTeams
     * @return \Illuminate\Http\Response
     */
    public function show(DepartmentTeams $departmentTeams)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DepartmentTeams  $departmentTeams
     * @return \Illuminate\Http\Response
     */
    public function edit(DepartmentTeams $departmentTeams)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DepartmentTeams  $departmentTeams
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DepartmentTeams $departmentTeams)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DepartmentTeams  $departmentTeams
     * @return \Illuminate\Http\Response
     */
    public function destroy(DepartmentTeams $departmentTeams)
    {
        //
    }
}
