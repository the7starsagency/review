<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DepartmentUsers extends Model
{
	protected $fillable = [
        'user_id','department_id'
    ];
	
    public static function departmentUsers( $department_id, $user_id )
	{
		$department_users = New DepartmentUsers();
		$department_users->department_id = $department_id;
		$department_users->user_id = $user_id;
		$department_users->save();
	}		
}
