<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
     protected $fillable = [
        'department_name','added_by'
    ];

	public function departmentUsers()
	{
		return $this->hasMany('App\DepartmentUsers');
	}
	public function departmentTeams()
	{
		return $this->hasMany('App\DepartmentTeams');
	}




}
