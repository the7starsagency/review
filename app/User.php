<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Notifications\ResetPasswordNotification;
use jeremykenedy\LaravelRoles\Traits\HasRoleAndPermission;
use View;

class User extends Authenticatable
{
    use Notifiable;
	use HasRoleAndPermission;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','status','profile_picture','password_reset_token','added_by',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

	public static function checkUserAuthentication( $token )
	{
		$user_data = User::where('password_reset_token','=',$token)->first();
		return $user_data;

	}

	public function comapnyDetail()
    {
        return $this->belongsTo('App\CompanyDetails','id','company_id');
    }
	public function usersInDepartment()
    {
        return $this->hasMany('App\DepartmentUsers');
    }
	public function usersInTeams()
    {
        return $this->hasMany('App\TeamUsers');
    }

    public function managerRole(){
        return $this->hasOne('App\RoleUser')->where('role_id','3');
    }
    public function peopleRole(){
        return $this->hasOne('App\RoleUser')->where('role_id','2');
    }
    public function adminRole(){
        return $this->hasOne('App\RoleUser')->where('role_id','1');
    }



}
