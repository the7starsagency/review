<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class TeamUsers extends Model
{
    protected $fillable = [
        'user_id','team_id'
    ];
	
	public static function teamUsers($user_id,$team_id)
	{
		$team_user = New TeamUsers();
		$team_user->user_id = $user_id;
		$team_user->team_id = $team_id;
		$team_user->save();
		
	}
	
	public function userDetailPeople()
	{
		return $this->hasOne('App\User','id','user_id')->whereHas('roles', function($q)  {
			$q->where('slug', 'people');
		});
	}
	
	public function userDetailManager()
	{
		return $this->hasOne('App\User','id','user_id')->whereHas('roles', function($q)  {
			$q->where('slug', 'manager');
		});
	}
	
}
