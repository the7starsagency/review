<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReviewTemplate extends Model
{
    protected $fillable = [
        'name','description','created_by'
    ];
	
	public static function saveReviewTemplate($user_id,$name,$descrption)
	{
		$review_template = new ReviewTemplate();
		$review_template->created_by = $user_id;
		$review_template->name = $name;
		$review_template->description = $descrption;
		$review_template->save();
		return $review_template->id;
	}
	
	public function reviewTemplateAttributeValues()
	{
		return $this->hasMany('App\ReviewTemplateAttribute')->orderBy('order','ASC');
	}
	
}
