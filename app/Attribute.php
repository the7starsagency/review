<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\AttributeValues;
use Illuminate\Database\Eloquent\SoftDeletes;

class Attribute extends Model
{
	use SoftDeletes;
	
	protected $dates = ['deleted_at'];
	 
    protected $fillable = [
        'attribute_name','score','is_evaluation_values','order'
    ];


    public function attributeValues(){

        return $this->hasMany('App\AttributeValues')->orderBy('order','ASC');
    }
	
	
}	
