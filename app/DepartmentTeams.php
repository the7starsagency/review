<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DepartmentTeams extends Model
{
    protected $fillable = [
        'department_id','team_id'
    ];
	
	
	public static function departmentTeams( $department_id, $team_id )
	{
		$dep_teams = New DepartmentTeams();
		$dep_teams->department_id = $department_id;
		$dep_teams->team_id = $team_id ;
		$dep_teams->save();	
	}
	
	public function departmentUsers()
	  {
		return $this->hasMany('App\Department');
	  }
	  
	 
}
