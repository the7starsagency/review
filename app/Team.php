<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $fillable = [
        'team_name','added_by'
    ];
	
	
	public function teamUsers()
  {
    return $this->hasMany('App\TeamUsers');
  }
  public function teamUsersPeople()
  {
    return $this->hasMany('App\TeamUsers')->whereHas('userDetailPeople');
  }
   public function teamUsersManager()
  {
    return $this->hasMany('App\TeamUsers')->whereHas('userDetailManager');
  }
  public function teamDepartments()
  {
    return $this->hasOne('App\DepartmentTeams');
  }
}
