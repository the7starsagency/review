<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class CompanyDetails extends Model 
{
    protected $fillable = [
        'company_id','company_name','subdomain','country_id','city','address'];
}
