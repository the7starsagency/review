<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReviewTemplateAttribute extends Model
{
     protected $fillable = [
        'review_template_id','review_template_id','review_template_id','review_template_id','require_self_evaluation','self_evaluation_instructions','order'
    ];
	
	
	public static function saveReviewTemplateAttribute( $id, $attr_values, $key )
	{
		$review_obj = 					new ReviewTemplateAttribute();
		$review_obj->review_template_id = $id;
		$review_obj->attribute_id 		= $attr_values['id'];
		if(isset( $attr_values['require_special_instructions']))
		{
			$review_obj->require_special_instructions	= '1';
			$review_obj->special_instructions 		  	= $attr_values['special_instructions'];
			
		}
		if(isset( $attr_values['require_self_evaluation']))
		{
			$review_obj->require_self_evaluation 		= '1';
			$review_obj->self_evaluation_instructions 	= $attr_values['self_evaluation_instructions'];
			
		}
		$review_obj->order 				= $key;
		$review_obj->save();
		return 0;	
		
	}
	
	public function reviewTemplateAttribute()
	{
		return $this->hasOne('App\Attribute','id','attribute_id')->withTrashed();
	}
}
