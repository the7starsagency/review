<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Config;
use Mail;
use File;
use App\User;
use Image;

class CommonFunctions extends Model
{

	public static function uploadsDirPath( $folder ) {
		
        $upload_dir = public_path() . '/upload/' . $folder;
		if (!File::exists($upload_dir)) {
            File::makeDirectory($upload_dir, $mode = 0777, true, true);
        }
        return $upload_dir;
    }
	
	public static function getAdMediaFolderPath( $id ) {
		
        $uploads_dir = self::uploadsDirPath('/profile/' . $id);
        return $uploads_dir;
		
	}
	
	public static function createDirPath( $dir_path ) {
		
        if ( !File::exists( $dir_path ) ) {
            File::makeDirectory( $dir_path, $mode = 0777, true, true );
        }
        return $dir_path;
    }
	
	public static function uploadFile( $file, $upload_dir, $custom_file_name = '' ) {
		
        $file_name = '';
        $docName = $file->getClientOriginalName();
        if ($file->isValid()) {
            $extension = $file->getClientOriginalExtension(); // getting image extension
            $fileWithoutExt = basename($docName, '.' . $extension);  //image name without extension
            $fileName = ($custom_file_name != '') ? $custom_file_name . '.' . $extension : $fileWithoutExt . '_' . rand(0, 1000) . '.' . $extension; // renameing image
            $data = $file->move($upload_dir, $fileName); // uploading file to given path
            if ($data > 0) {
                $file_name = $fileName;
            }
        }
		
        return $file_name;
    }
	
	public static function sendLarvelDefaultMail( $email_file_path,$data ) {
		
		$response=Mail::send($email_file_path,$data, function($message) use ($data){
			
			$message->to($data['email']);
			$message->subject($data['subject']);
		});
		return true;
	}
	
	public static function setActive($path, $active = 'active') {
		
		return call_user_func_array('Request::is', (array)$path) ? $active : '';
		
	}
	
	

}
