$(function() {
	
	$( ".form-group input" ).each(function( index ) {
		
		var val = $(this).val();
		if( val != "" ){
			
			$(this).closest('.form-line').addClass('focused');
		}
	   
	});
	
	$("input").blur();
	
	setTimeout(function(){ 
	
		$('.first-element').click();
		
	}, 1000);

});	