<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/test', function () {
		session()->flush();
		 Auth::logout();
		 header("Cache-Control: no-cache, no-store, must-revalidate");
		 header("Pragma: no-cache");
});


 Route::group(['domain' => 'smartfra.com'], function()
{
    Route::get('/register', ['as' => 'register', 'uses' => 'Auth\RegisterController@showRegistrationForm']);
    Route::post('/register', ['as' => 'register.post', 'uses' => 'Auth\RegisterController@register']);
	Route::get('/thank-you','Auth\RegisterController@thankYou')->name('company.thank-you');
	Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
	Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
	Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
	Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');
	Route::Post('/subdomain/','Auth\LoginController@validateSubdomain')->name('validate.subdomain');
});

Route::group(['domain' => '{company}.smartfra.com','middleware' => 'subdomain'], function($company){
	Route::get('/company/email/verify/{token}','Auth\LoginController@companyVerify')->name('company.verify');
	Route::get('login', ['as' => 'login', 'uses' => 'Auth\LoginController@showLoginForm']);
    Route::post('login', ['as' => 'login.post', 'uses' => 'Auth\LoginController@login']);
    Route::post('logout', ['as' => 'logout', 'uses' => 'Auth\LoginController@logout']);
	Route::post('/user/reset-password/','company\UserController@userResetPassword')->name('users.reset.password');
    Route::get('/user/reset-password/{token}','company\UserController@userResetToken')->name('users.reset.token');
	Route::get('/thank','Auth\LoginController@thank')->name('company.thank');
});

Route::group(['middleware' => ['auth','checkstatus']], function () {


	Route::group(['domain' => '{company}.smartfra.com','middleware' => ['subdomain','subdomainuser']], function($company){
		Route::get('profile', 'company\UserController@profile')->name('profile');
		Route::post('profile/update', 'company\UserController@updateProfile')->name('profile.update');
		Route::Post('company/subdomain','company\UserController@validateSubdomain')->name('company.validate.subdomain');
		Route::get('/dashboard', 'HomeController@index')->name('home');
		Route::get('user/{type}', 'company\UserController@index')->name('users');
		Route::get('user/{type}/create', 'company\UserController@create')->name('users.create');
		Route::post('user/{type}/store', 'company\UserController@store')->name('users.store');
		Route::Post('/validate-email','company\UserController@validateEmail')->name('validate-email');
		Route::get('user/{type}/edit/{id}', 'company\UserController@show')->name('users.show');
		Route::post('user/{type}/upadte/', 'company\UserController@update')->name('users.update');
        Route::post('user/{type}/delete','company\UserController@destroy')->name('users.destroy');
        Route::post('users/role', 'company\UserController@upgradePeople')->name('upgrade.people');
		Route::get('team', 'company\TeamController@index')->name('team');
		Route::get('team/create', 'company\TeamController@create')->name('team.create');
		Route::get('team/edit/{id}', 'company\TeamController@show')->name('team.edit');
		Route::post('team/update', 'company\TeamController@update')->name('team.update');
		Route::post('team/store', 'company\TeamController@store')->name('team.store');
		Route::post('team/delete','company\TeamController@destroy')->name('team.destroy');
		Route::get('department', 'company\DepartmentController@index')->name('department');
		Route::get('department/create', 'company\DepartmentController@create')->name('department.create');
		Route::post('department/store', 'company\DepartmentController@store')->name('department.store');
		Route::get('department/edit/{id}', 'company\DepartmentController@show')->name('department.edit');
		Route::post('department/update', 'company\DepartmentController@update')->name('department.update');
        Route::post('department/delete','company\DepartmentController@destroy')->name('department.destroy');
        Route::get('attribute', 'company\AttributeController@index')->name('attribute');
        Route::get('attribute/create', 'company\AttributeController@create')->name('attribute.create');
        Route::post('attribute/store', 'company\AttributeController@store')->name('attribute.store');
        Route::post('attribute/delete','company\AttributeController@destroy')->name('attribute.destroy');
        Route::get('attribute/edit/{id}', 'company\AttributeController@show')->name('attribute.edit');
        Route::post('attribute/update', 'company\AttributeController@update')->name('attribute.update');
        Route::post('clone-attribute', 'company\AttributeController@cloneAttribute')->name('clone.attribute');
        Route::get('template', 'company\ReviewTemplateController@index')->name('template');
        Route::get('template/create', 'company\ReviewTemplateController@create')->name('template.create');
        Route::post('add-attribute', 'company\ReviewTemplateController@addAttribute')->name('add.attribute');
		Route::post('template/store', 'company\ReviewTemplateController@store')->name('template.store');
		Route::post('template/delete','company\ReviewTemplateController@destroy')->name('template.destroy');
		Route::get('template/edit/{id}', 'company\ReviewTemplateController@show')->name('template.edit');
		Route::post('template/update', 'company\ReviewTemplateController@update')->name('template.update');



	});

});


