<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_details', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->unsignedBigInteger('company_id');
			$table->string('company_name',255)->nullable();
			$table->string('subdomain')->nullable();
			$table->integer('country_id')->nullable();
			$table->string('city',255)->nullable();
			$table->longText('address')->nullable();
			$table->timestamps();
        });
		Schema::table('company_details', function($table) {
		$table->foreign('company_id')->references('id')->on('users');
   });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_details');
    }
}
