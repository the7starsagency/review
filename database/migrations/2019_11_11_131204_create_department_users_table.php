<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDepartmentUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('department_users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
			$table->unsignedBigInteger('department_id');
            $table->timestamps();
        });
		
		Schema::table('department_users', function($table) {
            $table->foreign('user_id')->references('id')->on('users');
        });
		
		Schema::table('department_users', function($table) {
            $table->foreign('department_id')->references('id')->on('departments');
        });
		
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('department_users');
    }
}
