<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReviewTemplateAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('review_template_attributes', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->unsignedBigInteger('review_template_id');
			$table->unsignedBigInteger('attribute_id');
			$table->enum('require_special_instructions', ['1', '0'])->default('0');
            $table->string('special_instructions')->nullable();
			$table->enum('require_self_evaluation', ['1', '0'])->default('0');
            $table->string('self_evaluation_instructions')->nullable();
			$table->integer('order');
            $table->timestamps();
        });
		
		
		
		Schema::table('review_template_attributes', function($table) {
            $table->foreign('review_template_id')->references('id')->on('review_templates');
        });
		
		Schema::table('review_template_attributes', function($table) {
            $table->foreign('attribute_id')->references('id')->on('attributes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('review_template_attributes');
    }
}
