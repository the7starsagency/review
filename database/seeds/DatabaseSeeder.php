<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
         Eloquent::unguard();
		 DB::statement('SET FOREIGN_KEY_CHECKS=0;');
         $this->call(CountryTableSeeder::class);
		 $this->call(UsersTableSeeder::class);
		 $this->call(RolesTableSeeder::class);
		 $this->call(PermissionsTableSeeder::class);
         $this->call(ConnectRelationshipsSeeder::class);
		 DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
