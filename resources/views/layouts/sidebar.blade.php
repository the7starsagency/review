

<!-- <div class="user-info">
	<div class="image">

		<img src="{{Auth::user()->profile_picture ? asset('upload/profile/'.Auth::user()->id.'/'.Auth::user()->profile_picture) : asset('assets/images/xs/avatar1.jpg') }}" width="48" height="48" alt="User" />
	</div>
	<div class="info-container">
		<div class="name" data-toggle="dropdown">{{ Auth::user()->name }}</div>

		<div class="email">{{ Auth::user()->email }}</div>
	</div>
</div> -->

<div class="user-info">
        <div class="image">
		<img src="{{Auth::user()->profile_picture ? asset('upload/profile/'.Auth::user()->id.'/'.Auth::user()->profile_picture) : asset('assets/images/xs/avatar1.jpg') }}" width="48" height="48" alt="User" />
        </div>
        <div class="info-container">
			<div class="name" data-toggle="dropdown">{{ Auth::user()->name }}</div>
            <div class="btn-group user-helper-dropdown">
                <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" role="button"> keyboard_arrow_down </i>
                <ul class="dropdown-menu slideUp">
                    <li><a href="{{ route('profile',$company_name) }}"><i class="material-icons">person</i>Profile</a></li>
                   
                    <li class="divider"></li>
                    <!-- <li><a href="javascript:void(0);"><i class="material-icons">group</i>Followers</a></li>
                    <li><a href="javascript:void(0);"><i class="material-icons">shopping_cart</i>Sales</a></li>
                    <li><a href="javascript:void(0);"><i class="material-icons">favorite</i>Likes</a></li>
                    <li class="divider"></li> -->
                    <li><a href="{{ route('logout',$company_name) }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="material-icons">input</i>Sign Out</a></li>
                </ul>
            </div>
            <div class="email">{{ Auth::user()->email }}</div>
        </div>
    </div>


<div class="menu">
	<ul class="list">
		<li class="header">MAIN NAVIGATION</li>
		<li><a href="{{route('home',$company_name)}}" class = "{{request()->routeIs('home',$company_name) ? 'sa-active' : ''}}"><i class="zmdi zmdi-home"></i><span>Dashboard</span></a></li>
		@role('admin')

			<li><a class = "{{ (request()->segment(2) == 'people') ? 'sa-active' : '' }}"  href="{{route('users',[$company_name,'people'])}}"><i class="zmdi zmdi zmdi-account "></i><span>People</span> </a></li>
			<li><a  class = "{{ (request()->segment(2) == 'manager') ? 'sa-active' : '' }}" href="{{route('users',[$company_name,'manager'])}}"><i class="zmdi zmdi-account-box"></i><span>Manager</span> </a></li>
			<li><a class = "{{ (request()->segment(1) == 'team') ? 'sa-active' : '' }}"  href="{{route('team',$company_name)}}"><i class="zmdi zmdi-accounts"></i><span>Team</span> </a></li>
            <li><a class = "{{ (request()->segment(1) == 'department') ? 'sa-active' : '' }}"  href="{{route('department',$company_name)}}"><i class="zmdi  zmdi-border-all"></i><span>Departments</span> </a></li>
            <li><a class = "{{ (request()->segment(1) == 'attribute') ? 'sa-active' : '' }}"  href="{{route('attribute',$company_name)}}"><i class="zmdi zmdi-format-list-bulleted"></i>
                <span>Attribute</span> </a></li>
			<li><a class = "{{ (request()->segment(1) == 'template') ? 'sa-active' : '' }}"  href="{{route('template',$company_name)}}"><i class="zmdi zmdi-ungroup"></i>
                    <span>Review Templates</span> </a></li>
	    @endrole
	</ul>
</div>

