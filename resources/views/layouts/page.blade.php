<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>Review</title>
<!-- Favicon-->
<link rel="icon" href="favicon.ico" type="image/x-icon">
<!-- Custom Css -->
<link rel="stylesheet" href="{{asset('assets/plugins/bootstrap/css/bootstrap.min.css') }}">
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="{{asset('assets/plugins/bootstrap-select/css/bootstrap-select.css') }}">
<link rel="stylesheet" href="{{asset('assets/plugins/jvectormap/jquery-jvectormap-2.0.3.css') }}"/>
<link rel="stylesheet" href="{{asset('assets/plugins/morrisjs/morris.css') }}" />
<link href="{{asset('assets/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet" />
<link rel="stylesheet" href="{{asset('assets/css/main.css') }}">
<link rel="stylesheet" href="{{asset('assets/css/authentication.css') }}">
<link rel="stylesheet" href="{{asset('assets/css/color_skins.css') }}">
<link rel="stylesheet" href="{{asset('assets/css/custom.css') }}">

@yield('page_styles')

</head>

<body class="theme-orange">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">        
        <div class="line"></div>
		<div class="line"></div>
		<div class="line"></div>
        <p>Please wait...</p>
        <div class="m-t-30"><img src="{{asset('assets/images/logo.svg')}}" width="48" height="48" alt="Nexa"></div>
    </div>
</div>
<!-- Overlay For Sidebars -->
<div class="overlay"></div><!-- Search  -->
<!-- Top Bar -->
<nav class="navbar">
    <div class="col-12">        
        <div class="navbar-header">
            <a href="javascript:void(0);" class="bars"></a>
            <a class="navbar-brand" href="#">Review</a>
        </div>
        <ul class="nav navbar-nav navbar-left">
            <li><a href="javascript:void(0);" class="ls-toggle-btn" data-close="true"><i class="zmdi zmdi-swap"></i></a></li>
            
        </ul>
        <ul class="nav navbar-nav navbar-right">
          
            <li class="dropdown">
                <a href="javascript:void(0);" class="dropdown-toggle xs-hide" data-toggle="dropdown" role="button"><i class="zmdi zmdi-notifications"></i>
                    <div class="notify"><span class="heartbit"></span><span class="point"></span></div>
                </a>
                <ul class="dropdown-menu slideDown">
                    <li class="header">NOTIFICATIONS</li>
                    <li class="body">
                        <ul class="menu list-unstyled">
                            <li>
								<a href="javascript:void(0);">
									<div class="icon-circle l-coral"> <i class="material-icons">person_add</i> </div>
									<div class="menu-info">
										<h4>12 new members joined</h4>
										<p> <i class="material-icons">access_time</i> 14 mins ago </p>
									</div>
								</a>
							</li>
                            <li>
								<a href="javascript:void(0);">
									<div class="icon-circle l-turquoise"> <i class="material-icons">add_shopping_cart</i> </div>
									<div class="menu-info">
										<h4>4 sales made</h4>
										<p> <i class="material-icons">access_time</i> 22 mins ago </p>
									</div>
								</a>
							</li>
                            <li>
								<a href="javascript:void(0);">
									<div class="icon-circle g-bg-cyan"> <i class="material-icons">delete_forever</i> </div>
									<div class="menu-info">
										<h4><b>Nancy Doe</b> deleted account</h4>
										<p> <i class="material-icons">access_time</i> 3 hours ago </p>
									</div>
								</a>
							</li>
                           
                        </ul>
                    </li>
                    <li class="footer"><a href="javascript:void(0);">View All Notifications</a></li>
                </ul>
            </li>
                     
            <li>
				<a title = "Sign Out" href="{{ route('logout',$company_name) }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();" class="mega-menu xs-hide" data-close="true"><i class="zmdi zmdi-power"></i>
				</a>
			</li>
			    <form id="logout-form" action="{{ route('logout',$company_name) }}" method="POST" style="display: none;">
                  @csrf
               </form>
        </ul>
    </div>
</nav>

<!-- Left Sidebar -->
<aside id="leftsidebar" class="sidebar">

@include('layouts.sidebar')
</aside>

<section class="content home">
 <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Dashboard<small class="text-muted">
				Welcome to Review {{ (Auth::user()) ? Auth::user()->roles[0]->name : ''}}  panel</small></h2>
            </div>
            <!--<div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="#"><i class="zmdi zmdi-home"></i> Review</a></li>
                    <li class="breadcrumb-item active">Dashboard </li>
                </ul>-->
            </div>
        </div>
    </div>
@yield('content')

<div id="sa-loading">
	<div class="spinner-border" role="status">
	  <span class="sr-only">Loading...</span>
	</div>
</div>
<!-- Jquery Core Js -->

<script src="{{ asset('assets/bundles/libscripts.bundle.js') }}"></script> <!-- Lib Scripts Plugin Js -->
<!--<script src="{{ asset('assets/bundles/vendorscripts.bundle.js') }}"></script> <!-- Lib Scripts Plugin Js -->
<script src="{{ asset('assets/bundles/vendorscripts.js') }}"></script> <!-- Lib Scripts Plugin Js -->
<script src="{{ asset('assets/bundles/jvectormap.bundle.js') }}"></script> <!-- JVectorMap Plugin Js -->
<script src="{{ asset('assets/bundles/morrisscripts.bundle.js') }}"></script><!-- Morris Plugin Js -->
<script src="{{ asset('assets/bundles/sparkline.bundle.js') }}"></script> <!-- Sparkline Plugin Js -->
<script src="{{ asset('assets/bundles/knob.bundle.js') }}"></script> <!-- Jquery Knob Plugin Js -->
<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script><!-- Custom Js -->
<script src="{{ asset('assets/js/pages/index.js') }}"></script>
<script src="{{ asset('assets/js/pages/charts/jquery-knob.min.js') }}"></script>
<script src="{{ asset('assets/plugins/sweetalert/sweetalert.min.js') }}"></script> 
<script src="{{ asset('assets/js/pages/ui/dialogs.js') }}"></script>
<script src="{{ asset('js/custom.js') }}"></script>  
<script>
$(function() {
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
});
</script>
@yield('page_scripts')
</body>
</html>
