<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>Review</title>
<!-- Favicon-->
<link rel="icon" href="favicon.ico" type="image/x-icon">
<!-- Custom Css -->
<link rel="stylesheet" href="{{asset('assets/plugins/bootstrap/css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{asset('assets/plugins/bootstrap-select/css/bootstrap-select.css') }}">
<link rel="stylesheet" href="{{asset('assets/css/main.css') }}">
<link rel="stylesheet" href="{{asset('assets/css/authentication.css') }}">
<link rel="stylesheet" href="{{asset('assets/css/color_skins.css') }}">
<link rel="stylesheet" href="{{asset('assets/css/custom.css') }}">
 @yield('page_styles')
</head>

<body class="theme-orange">
<div class="authentication">
    <div class="card">
        <div class="body">
            <div class="row">
                <div class="col-lg-12">
                    <div class="header slideDown">
                        <div class="logo"><img src="{{asset('assets/images/logo.png')}}" alt="Nexa"></div>
                        <h1>Review Admin</h1>
                        
                    </div>
                </div>

                @yield('content')

            </div>
        </div>
    </div>
</div>
<div id="sa-loading">
	<div class="spinner-border" role="status">
	  <span class="sr-only">Loading...</span>
	</div>
</div>
<!-- Jquery Core Js -->
<script src="{{ asset('assets/bundles/libscripts.bundle.js') }}"></script> <!-- Lib Scripts Plugin Js -->
<script src="{{ asset('assets/bundles/vendorscripts.bundle.js') }}"></script> <!-- Lib Scripts Plugin Js -->
<!--<script src="{{ asset('assets/bundles/vendorscripts.js') }}"></script> <!-- Lib Scripts Plugin Js -->
<script src="{{ asset('assets/plugins/autosize/autosize.js') }}"></script> <!-- Autosize Plugin Js --> 
<script src="{{ asset('assets/plugins/momentjs/moment.js') }}"></script> <!-- Moment Plugin Js --> 
<!-- Bootstrap Material Datetime Picker Plugin Js --> 
<script src="{{ asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script> 
<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script><!-- Custom Js --> 
<script src="{{ asset('assets/js/pages/forms/basic-form-elements.js') }}"></script> 
<script src="{{ asset('js/custom.js') }}"></script>
<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script><!-- Custom Js -->
<script>
$(function() {
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
});
</script>

@yield('page_scripts')
</body>
</html>
