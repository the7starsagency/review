@extends('layouts.page')

@section('page_styles')
<style>
.register_form
{
	margin-top:20px;
}
 .authentication .card
{
	max-width:550px;
	margin-top: 100px;
}
.bootstrap-select.btn-group.show-tick>.btn,.bootstrap-select
{
	border-bottom:none !important;
}

</style>
@endsection

@section('page_scripts')
<script>
 
	$(function() {
		$('#profile_picture').on('change', function() {	
		   let fileName = $(this).val().split('\\').pop(); 
		   $(this).next('.custom-file-label').addClass("selected").html(fileName); 
		});
		
		 $("#company_name").on("change", function(){
       
			var str = $(this).val();
			// var regex = /[\. ,:-]+/g;
			var regex = /[\W_]/g;

			var result = str.replace(regex, '').toLowerCase();
			
			if(result != " ")
			{	$('#company_slug').parent('.form-line').addClass('focused');
				$('#company_slug').val(result);
				
			}
			
		});
		
		$("#company_slug").blur(function(){
			
			var val = $(this).val();
				
			$.ajax({
				url: "{{route('company.validate.subdomain', $company_name)}}",
				type: "post",
				data: { subdomain : val },
				success: function(data){
					console.log( data );
					if( data == 1)
					{
						$('#company_slug').parent('.form-line').addClass('error');
						$('#company_slug').addClass('is-invalid');
						$('.signup_btn').attr('disabled','disabled');
						
					} else{
						$('#company_slug').parent('.form-line').removeClass('error');
						$('#company_slug').removeClass('is-invalid');
						$('.signup_btn').removeAttr('disabled');
				}
					
				}
			});
		});
		
		
		
	});
	
	$('#sa-register-form').submit(function() {
        $('#sa-loading').show(); 
        return true;
    });
	
</script>
		
@endsection		

@section('content')


<div class="container-fluid">
    <div class="row clearfix">
		<div class="col-lg-12 col-md-12 col-sm-12">
			@if(session()->has('message'))
				<div class="alert alert-success">
					{{ session()->get('message') }}
				</div>
			@endif
			@if(session()->has('error'))
				<div class="alert alert-danger">
					{{ session()->get('error') }}
				</div>
			@endif
			<div class="card">
				<div class="header">
				<h5 class="float-left">Edit Profile</h5>
				</div>
				<div class="body">

					<form class="register_form" role="form"  class="login-form" action="{{ route('profile.update',$company_name) }}" method="POST" enctype="multipart/form-data" id = "sa-register-form" >
						@csrf
						<div class="row clearfix">
							<div class="col-lg-9">
							
								<div class="row clearfix">
									<div class="col-sm-6">
										<div class="form-group form-float">
											<div class="form-line">
												<input type="text" name="name" id="name" class="form-control first-element {{ $errors->has('name') ? ' is-invalid' : '' }}" required autocomplete="off" autofocus value="{{ old('name')? old('name') : ( ( isset( $profile->name ) && !empty ( $profile->name ) ) ? $profile->name : '' )  }}">
												<label class="form-label">Name </label>
												@if ($errors->has('name'))
												<span class="invalid-feedback" role="alert">
													<strong>{{ $errors->first('name') }}</strong>
												</span>
												@endif
											</div>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group form-float">
											<div class="form-line">
												<input type="email" class="form-control @error('email') is-invalid @enderror" readonly disabled value="{{ ( isset( $profile->email ) && !empty ( $profile->email ) ) ? $profile->email : old('email')  }}" required autocomplete="off">
												<label class="form-label">Email Address</label>
												@if ($errors->has('email'))
												<span class="invalid-feedback" role="alert">
													<strong>{{ $errors->first('email') }}</strong>
												</span>
												@endif
											</div>
										</div>
									</div>
								</div>
							
								<div class="row clearfix">
									<div class="col-sm-6">
										<div class="form-group form-float">
											<div class="form-line">
												<select id = "country" name ="country_id" class="form-control show-tick {{ $errors->has('country_id') ? ' is-invalid' : '' }}"  value="{{ old('country_id') }}" >
														<option value = "">Select Country</option>
														@foreach($countries as $country)
														<option {{ ( isset( $profile->comapnyDetail->country_id ) && !empty ( $profile->comapnyDetail->country_id ) && $profile->comapnyDetail->country_id ==  $country->id) ? 'selected' : ''  }}  value="{{$country->id}}">{{$country->name}}</option>
														@endforeach
												</select>
												
											</div>
										</div>
									</div>	
									<div class="col-sm-6">
										<div class="form-group form-float">
											<div class="form-line">
												<input type="text" class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}" name="city" value="{{ old('city')? old('city') : ( ( isset( $profile->comapnyDetail->city ) && !empty ( $profile->comapnyDetail->city ) ) ?$profile->comapnyDetail->city : '' )  }}"  autocomplete="off">

												<label class="form-label">City</label>
											</div>
										</div>
									</div>
								</div>
								
								<div class="row clearfix">
									<div class="col-sm-6">
										<div class="form-group form-float">
											<div class="form-line">
												<input type="text" required class="form-control {{ $errors->has('company_name') ? ' is-invalid' : '' }}" id = "company_name" name="company_name" value="{{ old('company_name')? old('company_name') : ( ( isset( $profile->comapnyDetail->company_name ) && !empty ( $profile->comapnyDetail->company_name ) ) ?$profile->comapnyDetail->company_name : '' )  }}"   autocomplete="off">

												<label class="form-label">Company Name</label>
												@if ($errors->has('company_name'))
												<span class="invalid-feedback" role="alert">
													<strong>{{ $errors->first('company_name') }}</strong>
												</span>
												@endif
											</div>
										</div>
									</div>
									<div class="col-sm-3">
										<div class="form-group form-float">
											<div class="form-line">

												<input type="text" required  id = "company_slug" class="form-control {{ $errors->has('subdomain') ? ' is-invalid' : '' }}" name="subdomain" value="{{ old('subdomain')? old('subdomain') : ( ( isset( $profile->comapnyDetail->subdomain ) && !empty ( $profile->comapnyDetail->subdomain ) ) ?$profile->comapnyDetail->subdomain : '' )  }}"  autocomplete="off">

												<label class="form-label">Subdomain</label>
												@if ($errors->has('subdomain'))
												<span class="invalid-feedback" role="alert">
													<strong>{{ $errors->first('subdomain') }}</strong>
												</span>
												@endif
											</div>
										</div>
									</div>
									<div class="col-sm-3 px-0">
										<div class="form-group form-float">
											<div class="form-line2">
												<input type="text" readonly class="form-control"  value=".smartfra.com" >
												
											</div>
										</div>
									</div>
								</div>
							
								<div class="row clearfix">
									<div class="col-sm-12">
										<div class="form-group form-float">
											<div class="form-line">
												<input type="text" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" value="{{ old('address')? old('address') : ( ( isset( $profile->comapnyDetail->address ) && !empty ( $profile->comapnyDetail->address ) ) ?$profile->comapnyDetail->address : '' )  }}"  autocomplete="off">
												<label class="form-label">Address</label>
											</div>
										</div>
									</div>
									
								</div>

							</div>
							
							<div class="col-lg-3">

								<div class="row clearfix">
									<div class="col-sm-12">
										<div class="form-group form-float">
										<div class="image">
											<img src="{{$profile->profile_picture ? asset('upload/profile/'.Auth::user()->id.'/'.$profile->profile_picture) : asset('assets/images/xs/avatar1.jpg') }}" width="100%" height="100%" alt="User" />
										</div>
										</div>
									</div>
								</div>

								<div class="row clearfix">
									<div class="col-sm-12">
										<div class="form-group form-float">
										<div class="form-line">
											<div class="custom-file">
												<input type="file" name="profile_picture" id = "profile_picture" accept="image/gif,image/jpeg,,image/png">
												<label for="profile_picture" class="custom-file-label text-truncate">Change Profie Picture...</label>
											</div>
										</div>
										</div>
									</div>
								</div>
							</div>
							
						</div>

						<div class="row clearfix">
							<div class="col-lg-12">
								<button type="submit" class="btn btn-raised btn-primary waves-effect signup_btn">SAVE</button>
								
							</div>
						</div>
									
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
