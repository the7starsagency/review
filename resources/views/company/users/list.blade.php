@extends('layouts.page')

@section('page_styles')

<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">

@endsection

@section('page_scripts')

<script src="{{asset('assets/bundles/datatablescripts.bundle.js')}}"></script>
<script src="{{asset('assets/plugins/jquery-datatable/buttons/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('assets/plugins/jquery-datatable/buttons/buttons.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/plugins/jquery-datatable/buttons/buttons.colVis.min.js')}}"></script>
<script src="{{asset('assets/plugins/jquery-datatable/buttons/buttons.flash.min.js')}}"></script>
<script src="{{asset('assets/plugins/jquery-datatable/buttons/buttons.html5.min.js')}}"></script>
<script src="{{asset('assets/plugins/jquery-datatable/buttons/buttons.print.min.js')}}"></script>

<script>

    $(document).ready( function () {

		var userTable = $('#laravel_datatable').DataTable({
			aaSorting: [[0, 'desc']],
			processing: true,
			serverSide: true,
		    ajax: "{{ route('users',[$company_name,$type]) }}",
		    columns: [
					{ data: 'DT_RowIndex', name: 'id' },
					{ data: 'name', name: 'name' },
					{ data: 'email', name: 'email' },
					{ data: 'action', name: 'action' },
				]
		});

		$(document).on('click','.data-delete', function (e) {
			e.preventDefault();
			var user_id = $(this).attr('data-id');
			showCancelMessage( user_id );

		});

		function showCancelMessage( user_id, ) {
		swal({
			title: "Are you sure?",
			text: "You want to delete this user!",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Yes, delete it!",
			cancelButtonText: "No, cancel it!",
			closeOnConfirm: false,
			closeOnCancel: false
		}, function (isConfirm) {
			if (isConfirm) {
				$.ajax({
					type : "POST",
					url: "{{route('users.destroy',[$company_name,$type] )}}",
					data : {'id' : user_id},
					success: function(data){
						if(data){
							swal("Deleted!", "User has been deleted.", "success");
							userTable.draw();
						} else {
							swal("Cancelled", "Error", "error");
						}
					}
				})
			} else {
				swal("Cancelled", "", "error");
			}
		});
		}
    });

</script>

@endsection

@section('content')
<div class="container-fluid">
	<div class="row clearfix">
		<div class="col-lg-12 col-md-12 col-sm-12">
		@if(session()->has('message'))
				<div class="alert alert-success">
					{{ session()->get('message') }}
				</div>
			@endif
			<div class="card">

			<div class="header">
				<div class = "col-sm-6">
					<h5 class = "float-left">Company {{ucfirst($type)}} </h5>
				</div>

				<div class = "col-sm-6 float-right">
                    @if($type == 'manager')
                        <a href = "{{route('users.create',[$company_name,$type])}}" class="btn  btn-raised btn-success waves-effect float-right">Add manager</a>
                    @else
                        <a href = "{{route('users.create',[$company_name,$type])}}" class="btn  btn-raised btn-success waves-effect float-right">Add {{ucfirst($type)}}</a>
                    @endif
				</div>

            </div>
			<div class="body">
					<div class="table-responsive">
						<table class="table table-bordered" id="laravel_datatable">
						   <thead>
							    <tr>
									<th>Id</th>
									<th>Name</th>
									<th>Email</th>
									<th>Action</th>

							    </tr>
						   </thead>
						</table>
					</div>
			</div>
			</div>
		</div>
	</div>

</div>
@endsection
