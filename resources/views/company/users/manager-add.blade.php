@extends('layouts.page')

@section('page_styles')


@endsection

@section('page_scripts')


<script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>
<script>
options = [];

	$(".custom-select").select2({
		tags: "true",
		placeholder: "Select an option",
		allowClear: true,
		width: '100%',
		createTag: function (params) {
			var term = $.trim(params.term);

			if (term === '') {
			  return null;
			}


			var search = $.grep(options, function( n, i ) {
			  return ( n.id === term || n.text === term);
			});


			if (search.length)
			  term = search[0].text;
			else
			  return null;

			return {
			 id: term,
			 text: term,
			 value: true
			}
	    }
	});




</script>

@endsection

@section('content')

<div class="container-fluid">
    <div class="row clearfix">
		<div class="col-lg-6 col-md-6 col-sm-6">
			<div class="card">
				<div class="header">
				<h5 class = "float-left">Add {{ucfirst($type)}} </h5>
				</div>
				<div class="body">

					<form action="{{ route('upgrade.people',$company_name) }}" method="POST" id = "sa-people-form" >
						@csrf
                        <div class="form-group">
                                <b>Upgrade people to manager</b>
                                <div class="form-line">
                                       <select class="custom-select" required value = ""   multiple="multiple" name = "upgarde_people[]" >

                                        @if(!empty($people_data))
                                            @foreach ($people_data as $people)
                                                <option value = "{{$people->id}}" > {{$people->email}}</option>
                                            @endforeach
                                        @endif

                                    </select>



                                </div>
                            </div>

                        <button type="submit" class="btn btn-raised btn-primary m-t-15 waves-effect" id = "sa-submit" @if( count($people_data) < 1 ){{ 'disabled'}} @endif>Add {{ucfirst($type)}}</button>
                        @if( count($people_data) < 1 )
                            <p class = "m-t-20">Note: Please add people first to assign manager</p>
                        @endif
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
