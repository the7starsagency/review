@extends('layouts.page')

@section('page_styles')

<link rel="stylesheet" href="{{asset('assets/css/taginput.css') }}">

@endsection

@section('page_scripts')

<script src="{{ asset('assets/js/inputtags.js') }}"></script>

<script>

$(function() {
	
	$('#sa-people-form').submit(function() {
		
        $('#sa-loading').show(); 
        return true;
		
    });
	
	
});

</script>

@endsection

@section('content')

<div class="container-fluid">        
    <div class="row clearfix">
		<div class="col-lg-12 col-md-12 col-sm-12">
			<div class="card">
				<div class="header">
				<h5 class = "float-left">Edit {{ucfirst($type)}} </h5>
				</div>
				<div class="body">
				
					<form action="{{route('users.update',[$company_name,$type])}}" method="POST" id = "sa-people-form" >
						@csrf
						<label for="name">Name</label>
						<div class="form-group">
							<div class="form-line">
								<input id="name" name="name" value = "{{$user->name}}" class="form-control  placeholder="" >
							</div>
						</div>

						<label for="email">Email</label>
						<div class="form-group">
							<input type = "hidden" value = "{{$user_id}}" name = "user_id" />
							<div class="form-line">
								<input readonly id="email" value = "{{$user->email}}" class="form-control  placeholder="" >
							</div>
						</div>
						
						@if( count($departments) > 0 )
							<div class="form-group">
								<h2 class="card-inside-title">Select Department</h2>
								@foreach( $departments as $dep )
									<input  type="checkbox" id="department_checkbox_{{$dep->id}}" class="filled-in" name = "departments[]" value = "{{$dep->id}}" @if(in_array($dep->id, $select_department)) {{"checked"}}@endif >
									<label for="department_checkbox_{{$dep->id}}">{{$dep->department_name}}</label>
								@endforeach
								
							</div>
						@endif
						
						@if( count($teams) > 0)
							<div class="form-group">
								<h2 class="card-inside-title">Select Team</h2>
								@foreach( $teams as $team )
									<input type="checkbox" id="team_checkbox_{{$team->id}}" class="filled-in" name = "teams[]" value = "{{$team->id}}" @if(in_array($team->id, $select_team)) {{"checked"}}@endif >
									<label for="team_checkbox_{{$team->id}}">{{$team->team_name}}</label>
								@endforeach
							</div>
						@endif
						
						@if( $type == 'people')
						<div class="form-group">
							<h2 class="card-inside-title">Select Manager</h2>
							
							<input type="checkbox"  id="is_manager"  class="filled-in" name = "is_manager" value = "1" @if(in_array(3, $manager_role)) {{"checked"}}@endif >
							<label for="is_manager">Is Manager</label>
								
						</div>
						@endif
						<button type="submit" class="btn btn-raised btn-primary m-t-15 waves-effect" id = "sa-submit">Update {{ucfirst($type)}}</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection