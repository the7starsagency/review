@extends('layouts.page')

@section('page_styles')

<link rel="stylesheet" href="{{asset('assets/css/taginput.css') }}">

@endsection

@section('page_scripts')

<script src="{{ asset('assets/js/inputtags.js') }}"></script>

<script>

$(function() {

	$("#sa-submit").click(function(e){

		e.preventDefault();
		var val = $('#email').val();
		if( val == "" ) {

			alert('Please Enter Email');

		} else {

			$("#sa-people-form").submit();
		}


	});

	var regex4 = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	$('#email').tagsInput({
		width: 'auto',
		pattern: regex4,
		defaultText:'',
		onAddTag: function(elem)
		{
			$.ajax({
				url: "{{route('validate-email',$company_name)}}",
				type: "post",
				data: { email : elem },
				success: function(data){
					if(data == 1)
					{
						$('#email').removeTag(elem);
						alert('User is already exist');
					}
				}
			});

		}


	});

	$('#sa-people-form').submit(function() {

        $('#sa-loading').show();
        return true;

    });
});

</script>

@endsection

@section('content')

<div class="container-fluid">
    <div class="row clearfix">
		<div class="col-lg-12 col-md-12 col-sm-12">
			<div class="card">
				<div class="header">
				<h5 class = "float-left">Add {{ucfirst($type)}} </h5>
				</div>
				<div class="body">

					<form action="{{ route('users.store',[$company_name,$type]) }}" method="POST" id = "sa-people-form" >
						@csrf
						<label for="email">Email Address ( Add Emails, comma separated )</label>
						<div class="form-group">
							<div class="form-line">
								<input type="text" id="email" class="form-control @error('email') is-invalid @enderror" placeholder="Add Emails" required  name = "email"  >
							</div>
						</div>

						@if( count($departments) > 0 )
							<div class="form-group">
								<h2 class="card-inside-title">Select Department</h2>
								@foreach( $departments as $dep )
									<input type="checkbox" id="department_checkbox_{{$dep->id}}" class="filled-in" name = "departments[]" value = "{{$dep->id}}" >
									<label for="department_checkbox_{{$dep->id}}">{{$dep->department_name}}</label>
								@endforeach

							</div>
						@endif

						@if( count($teams) > 0)
							<div class="form-group">
								<h2 class="card-inside-title">Select Team</h2>
								@foreach( $teams as $team )
									<input type="checkbox" id="team_checkbox_{{$team->id}}" class="filled-in" name = "teams[]" value = "{{$team->id}}">
									<label for="team_checkbox_{{$team->id}}">{{$team->team_name}}</label>
								@endforeach
							</div>
						@endif

						<button type="submit" class="btn btn-raised btn-primary m-t-15 waves-effect" id = "sa-submit">Add {{ucfirst($type)}}</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
