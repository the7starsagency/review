@extends('layouts.page')

@section('page_styles')

<link rel="stylesheet" href="{{asset('assets/plugins/nestable/jquery-nestable.css')}}"/>

@endsection

@section('page_scripts')


<script src="{{asset('assets/plugins/nestable/jquery.nestable.js')}}"></script>

<script>

var count = 1;

$(document).ready(function(){



	$('#evaluation').click(function(){

		if($(this).prop("checked") == true){

		   $('.attribute_value').show();
		}
		else {
			$('.attribute_value').hide();
		}
	});

    $('.dd').nestable({
        maxDepth:1,
    });

    $('.dd').on('change', function () {

        var $this = $(this);
        var serializedData = window.JSON.stringify($($this).nestable('serialize'));
		$this.parents('div.body').find('.order-output').val(serializedData);

    });

    $('.import-attrubute').click(function() {

        var attrubute_id = $(".sa-attribute option:selected").val();


        $.ajax({
				url: "{{route('clone.attribute',$company_name)}}",
				type: "post",
				data: { id : attrubute_id },
				success: function(data){

					if(data)
					{

                        jQuery.each( data.attribute, function( i, val ) {

                            if( data.is_value == '1' ) {
                                display = 'true';
                            }else{
                                if($('#evaluation').prop("checked") == false){
                                    display = 'false';
                                }else{
                                    display = 'true';
                                }
                            }
                            reaperter_div(count, val.score,val.description, display);
                            count++;
                        });

                    if( data.is_value == '1' ) {
                        // $('#evaluation').attr('checked','checked');
                        if($('#evaluation').prop("checked") == false){
                            $('#evaluation').click();
                        }

//                        $('.attribute_value').show();
                      }

					} if(data == 0){
                        showHtmlMessage(0);
                    }
                     if(data == -1){
                        showHtmlMessage(-1);
                    }
				}
			});

    });
 });

 function showHtmlMessage(val) {
     if(val == 0)
     {
         var title = 'Please select any attribute';
     }
     if(val == -1)
     {
         var title = 'Selected attribute does not have any value';
     }
    swal({
        title: "",
        text: title,
        html: true
    });
}


    $('.add').click(function() {

		// count = $('.repeat-values:last').attr('data-id');

		reaperter_div(count);
        count++;

		if($('#evaluation').prop("checked")){

				$('.attribute_value').show();

		} else{

				$('.attribute_value').hide();
		}
	});
	reaperter_div(0);

	function reaperter_div(count, dataValue='', dataDescription='', valueDisplay='false'){

        if(valueDisplay == 'true'){
            display = '';
        }else{
            display = 'style="display:none"';
        }
		$('.block:last').before('<li class="dd-item dd3-item repeat-values" data-id="'+count+'"><div class="dd-handle dd3-handle"></div><div class="dd3-content"><div class="block"><div class=" row clearfix"><div class="col-lg-3 nopadding attribute_value" '+display+' > <div class="form-group"><div class = "form-line">  <input type="number" class="form-control" id="values" name="attrValues['+count+'][values]" value="'+dataValue+'" placeholder="Value"></div> </div> </div> <div class="col-lg-6 nopadding"><div class="form-group"><div class = "form-line"> <input type="text" class="form-control" id="descrption"  name="attrValues['+count+'][descrption]" value="'+dataDescription+'" placeholder="Description"></div> </div></div><div class="col-lg-3 nopadding"><div class="form-group"><button type = "button" class="btn btn-raised btn-danger remove  waves-effect float-right">Remove</button></div></div></div></div> </div></div></li>');

	}

	$(document).on('click','.remove',function() {

		$(this).closest('.repeat-values').remove();
	});

</script>

@endsection

@section('content')

<div class="container-fluid">
    <div class="row clearfix">
		<div class="col-lg-6 col-md-6 col-sm-6">
			<div class="card">
				<div class="header">
				<h5 class = "float-left">Add Attribute </h5>
				</div>
				<div class="body">

					<form action="{{route('attribute.store',$company_name)}}" method="POST" id = "sa-people-form" >
						@csrf
					<div class="row clearfix">
                            <div class="col-sm-12">
                                <div class="form-group">
									<b>Attribute Name</b>
                                    <div class="form-line">
                                       <input type="text" name="attribute_name" id="attribute_name" class="form-control first-element {{ $errors->has('department_name') ? ' is-invalid' : '' }}" value="{{ old('department_name') }}" required autocomplete="off" autofocus placeholder = "">

										@if ($errors->has('attribute_name'))
											<span class="invalid-feedback" role="alert">
												<strong>{{ $errors->first('attribute_name') }}</strong>
											</span>
										@endif
                                    </div>
                                </div>
                                <div class="form-group">
									<b>Attribute Description</b>
                                    <div class="form-line">
                                        <textarea name="description" id="description" class="form-control first-element " value="{{ old('description') }}" ></textarea>

										@if ($errors->has('description'))
											<span class="invalid-feedback" role="alert">
												<strong>{{ $errors->first('description') }}</strong>
											</span>
										@endif
                                    </div>
                                </div>

								<div class="form-group">

                                    <div class="form-line3">
                                        <input type="checkbox" id="evaluation" class="filled-in" name="evaluation" value="1">
                                        <label for="evaluation">Add evaluations values</label>
									</div>
                                </div>

                                <div class="dd nestable-with-handle main-block">
                                        <ol class="dd-list">

                                            <div class="block">
                                                    <button type = "button" class="btn btn-raised btn-success add  waves-effect float-right">Add Value </button>
                                            </div>
                                        </ol>
                                </div>
                                <div class = "clearfix"></div>
                                @if( count($attribute) > 0 )
                                <div class="row clearfix m-t-20">
                                    <div class="col-lg-4">Import values from another attribute</div>
                                    <div class="col-lg-5">
                                        <select class="form-control show-tick sa-attribute">
                                            <option  value = "">Select an attribute</option>
											@if(!empty($attribute))
												@foreach ($attribute as $attr)
												    <option value = "{{$attr->id}}" > {{$attr->attribute_name}}</option>
                                                @endforeach
											@endif

                                        </select>
                                    </div>
                                    <div class="col-lg-3">

                                        <button type = "button" class="btn btn-raised btn-success import-attrubute  waves-effect float-right"  data-type="success">Import </button>

                                    </div>
                                </div>
                                @endif

							</div>


                    <div class="col-sm-12">

                        <button type="submit" class="btn btn-raised btn-primary m-t-15 waves-effect" id = "sa-submit">Add Attribute</button>
                    </div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
