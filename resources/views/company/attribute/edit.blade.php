@extends('layouts.page')

@section('page_styles')

<link rel="stylesheet" href="{{asset('assets/plugins/nestable/jquery-nestable.css')}}"/>

@endsection

@section('page_scripts')


<script src="{{asset('assets/plugins/nestable/jquery.nestable.js')}}"></script>

<script>

var count = <?php echo $count_value;?>;
$(document).ready(function(){
	$('#evaluation').click(function(){

		if($(this).prop("checked") == true){
		   $('.attribute_value').show();
		}
		else {
			$('.attribute_value').hide();
		}
	});

    $('.dd').nestable({

        maxDepth:1,
    });

    $('.dd').on('change', function () {

        var $this = $(this);
        var serializedData = window.JSON.stringify($($this).nestable('serialize'));
        $this.parents('div.body').find('.order-output').val(serializedData);

    });
 });

    $('.add').click(function() {

		//var count = $('.repeat-values:last').attr('data-id');

		reaperter_div(count);
        count++;

		if($('#evaluation').prop("checked")){

			$('.attribute_value').show();

		} else{

			$('.attribute_value').hide();
		}
	});

@if(isset($attribute->attributeValues) &&  count($attribute->attributeValues) > 0)

@else
    reaperter_div(0);
@endif

function reaperter_div(count){
    $('.block:last').before('<li class="dd-item dd3-item repeat-values" data-id="'+count+'"><div class="dd-handle dd3-handle"></div><div class="dd3-content"><div class="block"><div class=" row clearfix"><div class="col-lg-3 nopadding attribute_value {{$attribute->is_evaluation_values}}" {{($attribute->is_evaluation_values == 1) ? '' : 'style=display:none' }}> <div class="form-group"><div class = "form-line">  <input type="number" class="form-control" id="values" name="attrValues['+count+'][values]" value="" placeholder="values"></div> </div> </div> <div class="col-lg-6 nopadding"><div class="form-group"><div class = "form-line"> <input type="text" class="form-control" id="descrption"  name="attrValues['+count+'][descrption]" value="" placeholder="descrption"></div> </div></div><div class="col-lg-3 nopadding"><div class="form-group"> <div class="input-group2"><button type = "button" class="btn btn-raised btn-danger remove  waves-effect float-right">Remove</button></div></div></div></div> </div></div></li>');
}

$(document).on('click','.remove',function() {

 	$(this).closest('.repeat-values').remove();
});

</script>

@endsection

@section('content')

<div class="container-fluid">
    <div class="row clearfix">
		<div class="col-lg-6 col-md-6 col-sm-6">
			<div class="card">
				<div class="header">
				<h5 class = "float-left">Edit Attribute </h5>
				</div>
				<div class="body">

					<form action="{{route('attribute.update',$company_name)}}" method="POST" id = "sa-people-form" >
						@csrf
					<div class="row clearfix">
                            <div class="col-sm-12">
                                <div class="form-group">
									<b>Attribute Name</b>
                                    <div class="form-line">
                                    <input type = "hidden" value = "{{$attribute->id}}" name = "attribute_id">
                                       <input type="text" name="attribute_name" id="attribute_name" class="form-control first-element {{ $errors->has('department_name') ? ' is-invalid' : '' }}" value="{{isset($attribute->attribute_name) ? $attribute->attribute_name : ''}}" required autocomplete="off" autofocus placeholder = "">

										@if ($errors->has('attribute_name'))
											<span class="invalid-feedback" role="alert">
												<strong>{{ $errors->first('attribute_name') }}</strong>
											</span>
										@endif
                                    </div>
                                </div>
                                <div class="form-group">
									<b>Attribute Descrption</b>
                                    <div class="form-line">
                                        <textarea name="description" id="description" class="form-control first-element " value="" >{{isset($attribute->attribute_name) ? $attribute->description : ''}}</textarea>

										@if ($errors->has('description'))
											<span class="invalid-feedback" role="alert">
												<strong>{{ $errors->first('description') }}</strong>
											</span>
										@endif
                                    </div>
                                </div>

								<div class="form-group">

                                    <div class="form-line3">
                                        <input {{isset($attribute->is_evaluation_values) && ($attribute->is_evaluation_values == 1)  ? 'checked' : ''}} type="checkbox" id="evaluation" class="filled-in" name="evaluation" value="1">
                                        <label for="evaluation">Add evaluations values</label>
									</div>
                                </div>


                                <div class="dd nestable-with-handle main-block">
									<ol class="dd-list">

									@if(isset($attribute->attributeValues))
										@foreach( $attribute->attributeValues as $key=>$val)
											<li class="dd-item dd3-item repeat-values" data-id="{{ $key}}">
												<div class="dd-handle dd3-handle"></div>
												<div class="dd3-content">
													<div class="block">
														<div class=" row clearfix">
															<div class="col-lg-3 nopadding attribute_value" {{$attribute->is_evaluation_values}} {{($attribute->is_evaluation_values == 1) ? '' : 'style=display:none' }}>
																<div class="form-group">
																	<div class = "form-line">

																	<input type="number" class="form-control" id="values" name="attrValues[{{$key+1}}][values]" value="{{isset($val->score) ? $val->score: ''}}" placeholder="values">

																	</div>
																</div>
															</div>
															<div class="col-lg-6 nopadding">
																<div class="form-group">
																	<div class = "form-line">
																		<input type="text" class="form-control" id="descrption"  name="attrValues[{{$key+1}}][descrption]" value="{{isset($val->description) ? $val->description: ''}}" placeholder="descrption">
																	</div>
																</div>
															</div>
															<div class="col-lg-3 nopadding">
																<div class="form-group">
																	<div class="input-group2">
																		<button type = "button" class="btn btn-raised btn-danger remove  waves-effect float-right">Remove</button>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
                                            </li>
                                            <?php $count = $key;?>
										@endforeach

									@endif

									<div class="block">
											<button type = "button" class="btn btn-raised btn-success add  waves-effect float-right">Add Value </button>
										</div>
									</ol>
                                </div>


							</div>



						<div class="col-sm-12">
							<button type="submit" class="btn btn-raised btn-primary m-t-15 waves-effect" id = "sa-submit">Update Attribute</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
