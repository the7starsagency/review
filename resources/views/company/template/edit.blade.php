@extends('layouts.page')

@section('page_styles')

<link rel="stylesheet" href="{{asset('assets/plugins/nestable/jquery-nestable.css')}}"/>

@endsection

@section('page_scripts')


<script src="{{asset('assets/plugins/nestable/jquery.nestable.js')}}"></script>

<script>

var count = <?php echo $count+1;?>;
var added_attribute = [];
<?php if( isset( $review_temp->reviewTemplateAttributeValues ) ) {
	
	
			foreach( $review_temp->reviewTemplateAttributeValues as $key=>$val) {  ?>
				
				added_attribute.push( <?php echo '"'.$val->attribute_id.'"'; ?> );
		<?php 	}
}?>

$(document).ready(function(){
console.log(added_attribute);
		
        $(document).on("click",".specail_instruction",function() {
		
        if($(this).prop("checked") == true){

         
           $( this ).closest('.specail_instruction_main').next('.specail_instruction_div').show();
           
        }
        else {
           
            $( this ).closest('.specail_instruction_main').next('.specail_instruction_div').hide();
           
        }
    });
    $(document).on("click",".self_evaluation",function() {


        if($(this).prop("checked") == true){
        $( this ).closest('.self_evaluation_main').next('.self_evaluation_div').show();

        }
        else {
            $( this ).closest('.self_evaluation_main').next('.self_evaluation_div').hide();
        }
    });


$('.dd').nestable({
        maxDepth:1,
    });

    $('.dd').on('change', function () {

        var $this = $(this);
        var serializedData = window.JSON.stringify($($this).nestable('serialize'));
		$this.parents('div.body').find('.order-output').val(serializedData);

    });
		
		
	
    $('.add-attribute').click(function() {
	
        var attrubute_id = $(".sa-attribute option:selected").val();
		
		if( attrubute_id > 0){
			
		
			if( jQuery.inArray( attrubute_id, added_attribute )  !== -1 ){
				showHtmlMessage('Attribute is already exist');
			} else {
			
				added_attribute.push(attrubute_id);
				
				$.ajax({
						url: "{{route('add.attribute',$company_name)}}",
						type: "post",
						data: { id : attrubute_id },
						success: function(data){

							if(data.response > 0)
							{


								reaperter_div(count,data.attribute_name,data.id);
								count++;
								
							} 
						}
					});
			}
			
		}else{
			showHtmlMessage('Please select any attribute');
		}
		
    });
	
 });






	function reaperter_div(count,name,id){


		$('.dd-list').append('<li class="dd-item dd3-item repeat-values" data-id="'+count+'"><div class="dd-handle dd3-handle"></div><div class="dd3-content"><div class=" row clearfix"><div class = "col-lg-12 col-sm-12 col-md-12"><h5>  '+name+'</h5> </div><input type = "hidden" name = "attributes['+count+'][id]" value = "'+id+'" /><div class = "col-lg-12 col-sm-12 col-md-12 specail_instruction_main" ><div class="form-group"><div class="form-line2"><input type="checkbox" id="specail_instruction_'+count+'" class="filled-in specail_instruction" name="attributes['+count+'][require_special_instructions]" value="1"><label for="specail_instruction_'+count+'"">Add Specail Instructions</label></div></div></div><div class = "col-lg-12 col-sm-12 col-md-12 specail_instruction_div" style = "display:none;"><div class="form-group"><div class="form-line"><textarea name="attributes['+count+'][special_instructions]" class="form-control first-element" value=""></textarea> </div> </div> </div><div class = "col-lg-12 col-sm-12 col-md-12 self_evaluation_main"><div class="form-group"> <div class="form-line3"><input type="checkbox" id="self_evaluation_'+count+'" class="filled-in self_evaluation" name="attributes['+count+'][require_self_evaluation]" value="1"><label for="self_evaluation_'+count+'">Require Self Evaluations</label></div> </div></div><div class = "col-lg-12 col-sm-12 col-md-12 self_evaluation_div" style = "display:none;"><div class="form-group"><div class="form-line"><textarea name="attributes['+count+'][self_evaluation_instructions]" class="form-control " value=""></textarea></div></div></div></div><div class="form-group"><button type = "button" class="btn btn-raised btn-danger remove  waves-effect float-right" data-remove = "'+id+'">Remove</button></div></li>');

	}

	$(document).on('click','.remove',function() {
		console.log(added_attribute);
		var remove_ele = $(this).attr('data-remove');
		 added_attribute.splice($.inArray(remove_ele, added_attribute),1);
		$(this).closest('.repeat-values').remove();
	});
	function showHtmlMessage(message) {
   
    swal({
        title: "",
        text: message,
        html: true
    });
}

</script>

@endsection

@section('content')

<div class="container-fluid">
    <div class="row clearfix">
		<div class="col-lg-6 col-md-6 col-sm-6">
			<div class="card">
				<div class="header">
				<h5 class = "float-left">Edit Template </h5>
				</div>
				<div class="body">

					<form action="{{route('template.update',$company_name)}}" method="POST" id = "sa-people-form" >
						@csrf
					<div class="row clearfix">
                            <div class="col-sm-12">
                                <div class="form-group">
									<b>Template Name</b>
                                    <div class="form-line">
									<input type = "hidden" value = "{{$review_template_id}}" name = "id">
                                       <input type="text" name="name" id="name" class="form-control first-element {{ $errors->has('name') ? ' is-invalid' : '' }}" value="{{isset($review_temp->name) ? $review_temp->name : '' }}" required autocomplete="off" autofocus placeholder = "">

										@if ($errors->has('name'))
											<span class="invalid-feedback" role="alert">
												<strong>{{ $errors->first('name') }}</strong>
											</span>
										@endif
                                    </div>
                                </div>
                                <div class="form-group">
									<b>Template Description</b>
                                    <div class="form-line">
                                        <textarea name="description" id="description" class="form-control first-element " value="{{ old('description') }}" >{{isset($review_temp->description) ? $review_temp->description : '' }}</textarea>

										@if ($errors->has('description'))
											<span class="invalid-feedback" role="alert">
												<strong>{{ $errors->first('description') }}</strong>
											</span>
										@endif
                                    </div>
                                </div>



                                <div class = "clearfix"></div>
                                @if( count($attribute) > 0 )
                                <div class="row clearfix m-t-20">
                                    <div class="col-lg-4">Add Attribute</div>
                                    <div class="col-lg-5">
                                        <select class="form-control show-tick sa-attribute">
                                            <option  value = "">Select an attribute</option>
											@if(!empty($attribute))
												@foreach ($attribute as $attr)
												    <option value = "{{$attr->id}}" > {{$attr->attribute_name}}</option>
                                                @endforeach
											@endif

                                        </select>
                                    </div>
                                    <div class="col-lg-3">

                                        <button type = "button" class="btn btn-raised btn-success waves-effect float-right add-attribute"  data-type="success">Add </button>

                                    </div>
                                </div>
                                @endif
                                <div class="dd nestable-with-handle main-block">
                                        <ol class="dd-list">
                                        @if( isset( $review_temp->reviewTemplateAttributeValues ) )
											@foreach( $review_temp->reviewTemplateAttributeValues as $key=>$val)
											
											<li class="dd-item dd3-item repeat-values" data-id="{{$key}}">
												   <div class="dd-handle dd3-handle"></div>
												   <div class="dd3-content">
												   <div class=" row clearfix">
													  <div class = "col-lg-12 col-sm-12 col-md-12">
														 <h5>  {{$val->reviewTemplateAttribute['attribute_name']}}</h5>
													  </div>
													  <input type = "hidden" name = "attributes[{{$key+1}}][id]" value = "{{$val->attribute_id}}" />
													  <div class = "col-lg-12 col-sm-12 col-md-12 specail_instruction_main" >
													  
													
														 <div class="form-group">
															<div class="form-line2"><input type="checkbox" id="specail_instruction_{{$key+1}}" class="filled-in specail_instruction" value = "1" name="attributes[{{$key+1}}][require_special_instructions]" value=""{{($val['require_special_instructions'] == '1') ? 'checked' : ''}}><label for="specail_instruction_{{$key+1}}">Add Specail Instructions</label></div>
														 </div>
													  </div>
													  <div class = "col-lg-12 col-sm-12 col-md-12 specail_instruction_div" {{isset($val->special_instructions) ? '':'style=display:none'}}>
														 <div class="form-group">
															<div class="form-line"><textarea name="attributes[{{$key+1}}][special_instructions]" class="form-control first-element" value="">{{isset($val->special_instructions) ? $val->special_instructions:''}}</textarea> </div>
														 </div>
													  </div>
													  <div class = "col-lg-12 col-sm-12 col-md-12 self_evaluation_main">
													  
														 <div class="form-group">
															<div class="form-line3"><input type="checkbox" id="self_evaluation_{{$key+1}}" value = "1" class="filled-in self_evaluation" name="attributes[{{$key+1}}][require_self_evaluation]" value=""{{($val['require_self_evaluation'] == '1') ? 'checked' : ''}}><label for="self_evaluation_{{$key+1}}">Require Self Evaluations</label></div>
														 </div>
													  </div>
													  <div class = "col-lg-12 col-sm-12 col-md-12 self_evaluation_div" {{isset($val->self_evaluation_instructions) ? '':'style=display:none'}}>
														 <div class="form-group">
															<div class="form-line"><textarea name="attributes[{{$key+1}}][self_evaluation_instructions]" class="form-control " value="">{{isset($val->self_evaluation_instructions) ? $val->self_evaluation_instructions:''}}</textarea></div>
														 </div>
													  </div>
												   </div>
												   <div class="form-group"><button type = "button" class="btn btn-raised btn-danger remove  waves-effect float-right" data-remove = "{{$val->attribute_id}}">Remove</button></div>
												</li>	
												
												
											@endforeach
										@endif
                                        </ol>
                                </div>




							</div>


                    <div class="col-sm-12">

                        <button type="submit" class="btn btn-raised btn-primary m-t-15 waves-effect"  id = "sa-submit">Update Template</button>
                    </div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
