@extends('layouts.page')

@section('page_style')



@endsection

@section('page_scripts')


<script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>
<script>
	
	options = [];
	
	$(".custom-select").select2({
		tags: "true",
		placeholder: "Select a option",
		allowClear: true,
		width: '100%',
		createTag: function (params) {
			var term = $.trim(params.term);

			if (term === '') {
			  return null;
			}
			
			
			var search = $.grep(options, function( n, i ) {
			  return ( n.id === term || n.text === term);
			});
			
			
			if (search.length) 
			  term = search[0].text;
			else
			  return null; 
			
			return {
			 id: term,
			 text: term,
			 value: true 
			}
	    }
	});




</script>

@endsection

@section('content')

<div class="container-fluid">        
    <div class="row clearfix">
		<div class="col-lg-6 col-md-6 col-sm-6">
			<div class="card">
				<div class="header">
				<h5 class = "float-left">Edit Team </h5>
				</div>
				<div class="body">
				
					<form action="{{route('team.update',$company_name)}}" method="POST" id = "sa-people-form" >
						@csrf
					<div class="row clearfix">
                            <div class="col-sm-12">
                                <div class="form-group">
									<b>Team Name</b>
                                    <div class="form-line">
									   <input type = "hidden" name = "team_id" value = "{{$team_id}}" />
                                       <input type="text" name="team_name" id="team_name" class="form-control first-element {{ $errors->has('team_name') ? ' is-invalid' : '' }}" value="{{isset($team->team_name) ? $team->team_name : '' }}" required autocomplete="off" autofocus placeholder = " ">
										
										@if ($errors->has('team_name'))
											<span class="invalid-feedback" role="alert">
												<strong>{{ $errors->first('team_name') }}</strong>
											</span>
										@endif
                                    </div>
                                </div>
                                <div class="form-group">
									<b>Department</b>
                                    <div class="form-line">
                                       <select id = "department" name ="department" class="form-control show-tick"   >
											<option value = "">Select Department</option>
											
											@foreach($deapartment as $key=>$dep)
											  <option {{(isset($team->teamDepartments) && $team->teamDepartments->department_id == $dep->id ) ? 'selected' : '' }}  value="{{$dep->id}}">{{$dep->department_name}}</option>
											@endforeach
											
										</select>
                                    </div>
                                </div>
								
								<div class="form-group">
								    <b>People</b>
                                    <div class="form-line">
										<select class="custom-select"  multiple="multiple" name = "people[]" >
											
											@if(!empty($people_data))
												
												@foreach ($people_data as $key=>$people)
												
												<option @if(in_array($people->id, $select_people)) {{'selected'}}@endif value ="{{$people->id}}" > {{$people->email}}</option>
												  
											    @endforeach
										    @endif
										</select>
									</div>
								</div>
								
								<div class="form-group">
									<b>Manager </b>
                                    <div class="form-line">
										<select class="custom-select manager"  multiple="multiple" name = "manager[]" >
											
											
											
											@if(!empty($manager_data))
												
												@foreach ($manager_data as $key=>$manager)
											
											
												<option @if(in_array($manager->id, $select_manager)) {{'selected'}}@endif  value = "{{$manager->id}}" > {{$manager->email}}</option>
												
											 
												@endforeach
											@endif	
										</select>
									</div>
								</div>
								
							
								
								

                            </div>
                        </div>
						
						<button type="submit" class="btn btn-raised btn-primary m-t-15 waves-effect" id = "sa-submit">Update Team</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection