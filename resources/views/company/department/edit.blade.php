@extends('layouts.page')

@section('page_style')



@endsection

@section('page_scripts')


<script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>
<script>
	
	options = [];
	
	$(".custom-select").select2({
		tags: "true",
		placeholder: "Select an option",
		allowClear: true,
		width: '100%',
		createTag: function (params) {
			var term = $.trim(params.term);

			if (term === '') {
			  return null;
			}
			
			
			var search = $.grep(options, function( n, i ) {
			  return ( n.id === term || n.text === term);
			});
			
			
			if (search.length) 
			  term = search[0].text;
			else
			  return null; 
			
			return {
			 id: term,
			 text: term,
			 value: true 
			}
	    }
	});




</script>

@endsection

@section('content')

<div class="container-fluid">        
    <div class="row clearfix">
		<div class="col-lg-6 col-md-6 col-sm-6">
			<div class="card">
				<div class="header">
				<h5 class = "float-left">Edit Department </h5>
				</div>
				<div class="body">
				
					<form action="{{route('department.update',$company_name)}}" method="POST" id = "sa-people-form" >
						@csrf
					<div class="row clearfix">
                            <div class="col-sm-12">
                                <div class="form-group">
									<b>Department Name</b>
                                    <div class="form-line">
									 <input type = "hidden" name = "department_id" value = "{{$department_id}}" />
                                       <input type="text" name="department_name" id="department_name" class="form-control first-element {{ $errors->has('department_name') ? ' is-invalid' : '' }}" value="{{$department->department_name}}" required autocomplete="off" autofocus placeholder = "">
										
										@if ($errors->has('department_name'))
											<span class="invalid-feedback" role="alert">
												<strong>{{ $errors->first('department_name') }}</strong>
											</span>
										@endif
                                    </div>
                                </div>
                                <div class="form-group">
									<b>Department Lead</b>
                                    <div class="form-line">
                                       	<select class="custom-select"  multiple="multiple" name = "department_lead[]" >
											
											@if(!empty($manager_data))
												@foreach ($manager_data as $manager)
												<option @if(in_array($manager->id, $select_users)) {{'selected'}}@endif value = "{{$manager->id}}" > {{$manager->email}}</option>
											  
												@endforeach
											@endif
											
										</select>
                                    </div>
                                </div>
								
								<div class="form-group">
									<b>Teams</b>
                                    <div class="form-line">
										<select class="custom-select"  multiple="multiple" name = "teams[]" >
										
											@if(!empty($teams_data))
												
												@foreach ($teams_data as $key=>$team)
												
												<option @if(in_array($team->id, $select_team)) {{'selected'}}@endif value ="{{$team->id}}" > {{$team->team_name}}</option>
												  
											    @endforeach
										    @endif
										</select>
									</div>
								</div>
								<div class="form-group">
									<b>Department Members</b>
                                    <div class="form-line">
										<select class="custom-select"  multiple="multiple" name = "department_members[]" >
											
											@if(!empty($people_data))
												
												@foreach ($people_data as $people)
												
												<option @if(in_array($people->id, $select_users)) {{'selected'}}@endif value ="{{$people->id}}" > {{$people->email}}</option>
												  
											    @endforeach
										    @endif
										</select>
									</div>
								</div>
								
							
								
								

                            </div>
                        </div>
						
						<button type="submit" class="btn btn-raised btn-primary m-t-15 waves-effect" id = "sa-submit">Update Department</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection