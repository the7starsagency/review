@extends('layouts.auth')

@section('content')
<div class = "col-sm-12">
<p class = "text-center">Thank you for reset the password</p>

<a class="btn btn-raised btn-primary waves-effect" href = "{{route('login',$company_name)}}" >Login</a>
</div>

@endsection
