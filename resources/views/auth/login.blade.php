@extends('layouts.auth')

@section('content')
<form class="col-lg-12" id="sign_in" action="{{ route('login',$company_name) }}" method = "POST">
 @csrf
	<h5 class="title">Sign in to your Account</h5>
	@if(session()->has('message') )
		<div class="alert alert-success">
			{{ session()->get('message') }}
		</div>
	@endif
	@if((isset($_GET['success']) && $_GET['success'] == 1) )
		<div class="alert alert-success">
			Thank you for activating your account. You can login now.
		</div>
	@endif
	<div class="form-group form-float">
		<div class="form-line">
			<input type="text" class="form-control @error('email') is-invalid @enderror first-element" name="email" value="{{ old('email') }}" required autocomplete="off" autofocus">
			<label class="form-label">Email</label>
			@error('email')
				<span class="invalid-feedback" role="alert">
					<strong>{{ $message }}</strong>
				</span>
            @enderror

		</div>

	</div>
	<div class="form-group form-float">
		<div class="form-line">
			<input type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="off">
			<label class="form-label">Password</label>
			@error('password')
				<span class="invalid-feedback" role="alert">
					<strong>{{ $message }}</strong>
				</span>
            @enderror
		</div>
	</div>
	<div>
		<input type="checkbox" name="remember" id="rememberme" class="filled-in chk-col-cyan" {{ old('remember') ? 'checked' : '' }}>
		<label for="rememberme">Remember Me</label>
	</div>

	<div class="col-lg-12">
		<button type="submit" class="btn btn-raised btn-primary waves-effect">SIGN IN</button>
		
	</div>
</form>

<div class="col-lg-12 m-t-20">
		<a class="" href="{{ route('password.request') }}">Forgot Password?</a>
	</div>
@endsection
