@extends('layouts.auth')
@section('content')
	<div class="col-lg-12">
		<h5 class="title">{{ __('Verify Your Email Address') }}</h5>	
		<div class="card-body">
			@if (session('resent'))
				<div class="alert alert-success" role="alert">
					{{ __('A fresh verification link has been sent to your email address.') }}
				</div>
			@endif

			{{ __('Before proceeding, please check your email for a verification link.') }}
			{{ __('If you did not receive the email') }}
			<form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
				@csrf
				<button type="submit" class="btn btn-raised btn-primary waves-effect">{{ __('Click here to request another') }}</button>
			</form>
			
		</div>
	</div> 
@endsection
