@extends('layouts.auth')


@section('content')

@if(!empty($user))
	<form class="col-lg-12" id="sign_in" method="POST" action="{{ route('users.reset.password',$company_name) }}">
		@csrf
		 <input type="hidden" name="user_id" value="{{$user->id}}">
		  
			<small class="msg"> Activate Your Account.</small>
			
			<div class="form-group form-float">
				<div class="form-line">
					<input id="email" type="email" class="form-control" value = "{{$user->email}}"   autocomplete="email" readonly autofocus>
					<label class="form-label">Email</label>	
				</div>
			</div>
			
			<div class="form-group form-float">
				<div class="form-line">
					<input id="name" type="name" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $name ?? old('name') }}" required autocomplete="name" autofocus>
					<label class="form-label">Name</label>	
					
					@error('name')
						<span class="invalid-feedback" role="alert">
							<strong>{{ $message }}</strong>
						</span>
					@enderror
					
				</div>
			</div>
			
			
			<div class="form-group form-float">
				<div class="form-line">
					<input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
					<label class="form-label">New Password</label>	
					@error('password')
						<span class="invalid-feedback" role="alert">
							<strong>{{ $message }}</strong>
						</span>
					@enderror
					
				</div>
			</div>
			<div class="form-group form-float">
				<div class="form-line">
					<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
					<label class="form-label">Confirm Password</label>
				</div>
			</div>
				
			<div class="col-lg-12">
				<button type="submit" class="btn btn-raised btn-primary waves-effect">Reset my password</button>	
			
			</div>
	</form>
@else
	<div class="col-lg-12">
		<p class = "text-center">Invalid Url</p>
	</div>

@endif	
		
@endsection
