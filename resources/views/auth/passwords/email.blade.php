@extends('layouts.auth')

@section('content')
<form class="col-lg-12" id="sign_in"method="POST" action="{{ route('password.email') }}">
		@csrf
		<h5 class="title">Forgot Password?</h5>
		<small class="msg">Enter your e-mail address below to reset your password.</small>
		@if (session('status'))
			<div class="alert alert-success" role="alert">
				{{ session('status') }}
			</div>
		@endif
		<div class="form-group form-float">
				<div class="form-line">
					 <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

					<label class="form-label">Email</label>
					 @error('email')
						<span class="invalid-feedback" role="alert">
							<strong>{{ $message }}</strong>
						</span>
					@enderror
					
				</div>
		</div>
			
		<div class="col-lg-12">
			<button type="submit" class="btn btn-raised btn-primary waves-effect">{{ __('Reset my password') }}</button>	
		
		</div>
</form>
                
		<div class="col-lg-12 m-t-20">
			<!--<a href="#" title="">Sign In!</a>-->
		</div>
@endsection
