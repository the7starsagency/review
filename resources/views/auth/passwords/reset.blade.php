@extends('layouts.auth')

@section('content')

<form class="col-lg-12" id="sign_in" method="POST" action="{{ route('password.update') }}">
	@csrf
	 <input type="hidden" name="token" value="{{ $token }}">
        <h5 class="title">Reset Password?</h5>
		<small class="msg"> Reset your password.</small>
		<div class="form-group form-float">
				<div class="form-line">
				
					<input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>
					<label class="form-label">Email</label>	
					
					@error('email')
						<span class="invalid-feedback" role="alert">
							<strong>{{ $message }}</strong>
						</span>
					@enderror
					
				</div>
		</div>
		
		<div class="form-group form-float">
				<div class="form-line">
					
					<input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
					<label class="form-label">New Password</label>	
					@error('password')
						<span class="invalid-feedback" role="alert">
							<strong>{{ $message }}</strong>
						</span>
					@enderror
					
				</div>
		</div>
		<div class="form-group form-float">
			<div class="form-line">
			<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
				<label class="form-label">Confirm Password</label>
			</div>
		</div>
			
		<div class="col-lg-12">
			<button type="submit" class="btn btn-raised btn-primary waves-effect">{{ __('Reset my password') }}</button>	
		
		</div>
</form>
                
		
@endsection
