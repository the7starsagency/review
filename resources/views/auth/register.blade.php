@extends('layouts.auth')

@section('page_styles')
<style>
.register_form
{
	margin-top:20px;
}
 .authentication .card
{
	max-width:550px;
	margin-top: 100px;
}
.bootstrap-select.btn-group.show-tick>.btn,.bootstrap-select
{
	border-bottom:none !important;
}

</style>
@endsection

@section('page_scripts')
<script>
 
	$(function() {
		$('#profile_picture').on('change', function() {	
		   let fileName = $(this).val().split('\\').pop(); 
		   $(this).next('.custom-file-label').addClass("selected").html(fileName); 
		});
		
		 $("#company_name").on("change", function(){
       
			var str = $(this).val();
			// var regex = /[\. ,:-]+/g;
			var regex = /[\W_]/g;

			var result = str.replace(regex, '').toLowerCase();
			
			if(result != " ")
			{	$('#company_slug').parent('.form-line').addClass('focused');
				$('#company_slug').val(result);
				
			}
			
		});
		
		$("#company_slug").blur(function(){
			
			var val = $(this).val();
		
				
			$.ajax({
				url: "{{route('validate.subdomain')}}",
				type: "post",
				data: { subdomain : val },
				success: function(data){
					
					if( data == 1)
					{
						$('#company_slug').parent('.form-line').addClass('error');
						$('#company_slug').addClass('is-invalid');
						$('.signup_btn').attr('disabled','disabled');
						
					} else{
						$('#company_slug').parent('.form-line').removeClass('error');
						$('#company_slug').removeClass('is-invalid');
						$('.signup_btn').removeAttr('disabled');
				}
					
				}
			});
		});
		
		
		
	});
	
	$('#sa-register-form').submit(function() {
        $('#sa-loading').show(); 
        return true;
    });
	
</script>
		
@endsection		

@section('content')


<form class="col-lg-12 register_form" role="form"  class="login-form" action="{{ route('register') }}" method="POST" enctype="multipart/form-data" id = "sa-register-form" >
    @csrf
	<div class="row clearfix">
		<div class="col-sm-6">
			<div class="form-group form-float">
				<div class="form-line">
					<input type="text" name="name" id="name" class="form-control first-element {{ $errors->has('name') ? ' is-invalid' : '' }}" value="{{ old('name') }}" required autocomplete="off" autofocus>
					<label class="form-label">Name </label>
					@if ($errors->has('name'))
					<span class="invalid-feedback" role="alert">
						<strong>{{ $errors->first('name') }}</strong>
					</span>
					@endif
				</div>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group form-float">
				<div class="form-line">
					<input type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="off">
					<label class="form-label">Email Address</label>
					@if ($errors->has('email'))
					<span class="invalid-feedback" role="alert">
						<strong>{{ $errors->first('email') }}</strong>
					</span>
					@endif
				</div>
			</div>
		</div>
	</div>
	<div class="row clearfix">
		<div class="col-sm-6">
			<div class="form-group form-float">
				<div class="form-line">
					<input type="password" id= "password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" required name="password"  autocomplete="false">
					<label class="form-label">Password (Must be 8 character)</label>
					@if ($errors->has('password'))
					<span class="invalid-feedback" role="alert">
						<strong>{{ $errors->first('password') }}</strong>
					</span>
					@endif
				</div>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group form-float">
				<div class="form-line">
					<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="false">
					 <label class="form-label">Confirm Password</label>
					@if ($errors->has('password_confirmation'))
					<span class="invalid-feedback" role="alert">
						<strong>{{ $errors->first('password_confirmation') }}</strong>
					</span>
					@endif
				</div>
			</div>
		</div>
	</div>
	
	<div class="row clearfix">
		<div class="col-sm-6">
			<div class="form-group form-float">
				<div class="form-line">
					<select id = "country" name ="country_id" class="form-control show-tick {{ $errors->has('country_id') ? ' is-invalid' : '' }}"  value="{{ old('country_id') }}" >
							<option value = "">Select Country</option>
							@foreach($countries as $country)
							  <option {{old('country_id') == $country->id ? 'selected':'' }}  value="{{$country->id}}">{{$country->name}}</option>
							@endforeach
					</select>
					 
				</div>
			</div>
		</div>	
		<div class="col-sm-6">
			<div class="form-group form-float">
				<div class="form-line">
					<input type="text" class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}" name="city" value="{{ old('city') }}"  autocomplete="off">
					<label class="form-label">City</label>
				</div>
			</div>
		</div>
	</div>
	<div class="row clearfix">
		<div class="col-sm-6">
			<div class="form-group form-float">
				<div class="form-line">
					<input type="text" required class="form-control {{ $errors->has('company_name') ? ' is-invalid' : '' }}" id = "company_name" name="company_name" value="{{ old('company_name') }}"   autocomplete="off">
					<label class="form-label">Company Name</label>
					@if ($errors->has('company_name'))
					<span class="invalid-feedback" role="alert">
						<strong>{{ $errors->first('company_name') }}</strong>
					</span>
					@endif
				</div>
			</div>
		</div>
		<div class="col-sm-3">
			<div class="form-group form-float">
				<div class="form-line">
					<input type="text" required  id = "company_slug" class="form-control {{ $errors->has('company_name') ? ' is-invalid' : '' }}" name="subdomain" value="{{ old('subdomain') }}"  autocomplete="off">
					<label class="form-label">Subdomain</label>
					
				</div>
			</div>
		</div>
		<div class="col-sm-3 px-0">
			<div class="form-group form-float">
				<div class="form-line2">
					<input type="text" readonly class="form-control"  value=".smartfra.com" >
					
				</div>
			</div>
		</div>
	</div>
	
	

	<div class="form-group form-float">
		<div class="form-line">
			<input type="text" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" value="{{ old('address') }}"  autocomplete="off">
			<label class="form-label">Address</label>
		</div>
	</div>


	<div class="form-group form-float">
		<div class="form-line">
			<div class="custom-file">
				<input type="file" name="profile_picture" id = "profile_picture" accept="image/gif,image/jpeg,,image/png">
				<label for="profile_picture" class="custom-file-label text-truncate">Choose Profie Picture...</label>
			</div>
			
		</div>
	</div>
				 
	
	<div class="col-lg-12">
		<button type="submit" class="btn btn-raised btn-primary waves-effect signup_btn">SIGN UP</button>
		
	</div>
	<div class="col-lg-12 m-t-20">
		<a class="" href="{{ route('password.request') }}">Forgot Password?</a>
	</div>
				
</form>
	
	


@endsection
